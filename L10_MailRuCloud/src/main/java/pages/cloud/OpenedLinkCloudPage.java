package pages.cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OpenedLinkCloudPage extends CloudPageSwitcher {

    public OpenedLinkCloudPage(WebDriver driver) {
        super(driver);
    }

    public String verifyThatLinkOpensFile(String fileName) {

        WebElement uploadedFile = driver.findElement(By.xpath("//a[@data-name='link-action']"));
        return uploadedFile.getText();
    }
}
