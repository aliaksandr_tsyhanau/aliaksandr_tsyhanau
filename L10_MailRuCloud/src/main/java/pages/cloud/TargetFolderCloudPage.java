package pages.cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static pages.cloud.MainCloudCloudPage.getUploadedFileIconLocator;

public class TargetFolderCloudPage extends CloudPageSwitcher {

    @FindBy(xpath = "//a[@data-name='publish']")
    public WebElement getLinkButton;
    @FindBy(xpath = "//button[@data-name='popup']")
    public WebElement openLinkButton;

    public TargetFolderCloudPage(WebDriver driver) {
        super(driver);
    }

    public OpenedLinkCloudPage openLinkOfUploadedFile(String fileName) {
        WebElement uploadedFile = driver.findElement(By.xpath(String.format(getUploadedFileIconLocator(), fileName)));
        wait.until(ExpectedConditions.visibilityOf(uploadedFile));
        new Actions(driver).contextClick(uploadedFile).click(getLinkButton).build().perform();
        click(openLinkButton);
                for(String winHandle : driver.getWindowHandles()){
                    driver.switchTo().window(winHandle);
                }
        return  new OpenedLinkCloudPage(driver);
    }
}
