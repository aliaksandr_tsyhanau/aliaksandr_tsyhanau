package pages.cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.AbstractPage;
import java.util.List;

public class CloudPageSwitcher extends AbstractPage {

    public static final int TEN = 10;
    public static final int SIXTY = 60;

    public CloudPageSwitcher(WebDriver driver) {
        super(driver);
    }


    public boolean checkThatFolderOrFileIsCreated(String newFolderName, String pathToWaitElement) {
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(pathToWaitElement)));
        List<WebElement>  elements = driver.findElements(
                By.xpath("//div[contains(@class, 'b-collection__item_datalist')]"));

        for (WebElement element : elements) {
            //System.out.println("Element get " + element.getText());
            if (element.getText().contains(newFolderName)) {
                return true;
            }
        }
        return false;
    }


}
