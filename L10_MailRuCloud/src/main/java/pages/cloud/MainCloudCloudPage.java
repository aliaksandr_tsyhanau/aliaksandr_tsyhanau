package pages.cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MainCloudCloudPage extends CloudPageSwitcher {

    private static final String CHECK_BOX_FOLDER_LOCATOR =
            "//div[text()='%s']/ancestor::div[contains("
                    + "@class, 'b-thumb__controlbar')]//div[contains(@class, 'b-checkbox__box')]";
    private static final String UPLOADED_FILE_LOCATOR = "//span[text()='%s']";
    private static final String UPLOADED_FILE_ICON_LOCATOR =
            "//span[text()='%s']/ancestor::div[contains(@class, 'b-thumb b-thumb_datalist')]";


    @FindBy(xpath = "//input[@type='file' and @class='layer_upload__controls__input']")
    private WebElement uploadFileAlertButton;
    @FindBy(xpath = "//div[@id='toolbar-left']//div[@data-name='upload']")
    private WebElement uploadFileButton;
    @FindBy(xpath = "(//div[@data-name='remove'])[1]")
    private WebElement deleteButton;
    @FindBy(id = "PH_user-email")
    private WebElement emailLabel;
    @FindBy(xpath = "//div[@id='toolbar-left']//div[@data-mnemo='ctrl']")
    private WebElement createNewSelectButton;
    @FindBy(xpath = "//*[@id='toolbar-left']//a[@data-name='folder']/span")
    private WebElement chooseFolderInSelectButton;
    @FindBy(xpath = "//input[@class='layer__input']")
    private WebElement newFolderNameInput;
    @FindBy(xpath = "//button[@data-name='add']")
    private WebElement newFolderAddButton;
    //@FindBy(xpath = "//span[@class='b-filename__name']")
    //WebElement namesOfFolders;//
    @FindBy(xpath = "//button[@data-name='remove']")
    private WebElement alertDeleteButton;
    @FindBy(xpath = "(//button[@data-name='close'])[7]")
    private WebElement confirmButton;
    @FindBy(xpath = "//div[text()='Mail.Ru рекомендует']/ancestor::div[contains(@class, 'b-thumb_first')]")
    WebElement targetFolder;
    @FindBy(xpath = "(//button[@data-name='move'])[1]")
    WebElement moveConfirmButton;

    public MainCloudCloudPage(WebDriver driver) {
        super(driver);
    }

    public static String getUploadedFileIconLocator() {
        return UPLOADED_FILE_ICON_LOCATOR;
    }

    public String getEmailText() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_user-email")));
        return emailLabel.getText();
    }

    public void createNewFolder(String folderName) {
        click(createNewSelectButton);
        wait.until(ExpectedConditions.elementToBeClickable(chooseFolderInSelectButton));
        click(chooseFolderInSelectButton);
        wait.until(ExpectedConditions.elementToBeClickable(newFolderNameInput));
        newFolderNameInput.sendKeys(folderName);
        wait.until(ExpectedConditions.visibilityOf(newFolderNameInput));
        click(newFolderAddButton);
    }

    public void deleteFolder(String newFolderName) {
        WebElement checkbox = driver.findElement(By.xpath(String.format(CHECK_BOX_FOLDER_LOCATOR, newFolderName)));
        wait.until(ExpectedConditions.visibilityOf(checkbox));
        click(checkbox);
        wait.until(ExpectedConditions.visibilityOf(deleteButton));
        click(deleteButton);

        wait.until(ExpectedConditions.visibilityOf(alertDeleteButton));
        alertDeleteButton.click();
        //wait.until(ExpectedConditions.visibilityOf(confirmButton));
        if (confirmButton.isDisplayed()) {
            click(confirmButton);
        }
    }

    public void uploadFile(String pathToFile, String fileName) {
        click(uploadFileButton);
        uploadFileAlertButton.sendKeys(pathToFile);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath(String.format(UPLOADED_FILE_LOCATOR, fileName))));
    }

    public void dragAndDropFile(String fileName) {
        WebElement draggable = driver.findElement(By.xpath(String.format(UPLOADED_FILE_ICON_LOCATOR, fileName)));
        wait.until(ExpectedConditions.visibilityOf(draggable));
        wait.until(ExpectedConditions.visibilityOf(targetFolder));
        new Actions(driver).dragAndDrop(draggable, targetFolder).perform();
        wait.until(ExpectedConditions.visibilityOf(moveConfirmButton));
        click(moveConfirmButton);
    }

    public TargetFolderCloudPage openTargetFolder() {
        targetFolder.click();
        return new TargetFolderCloudPage(driver);
    }
}
