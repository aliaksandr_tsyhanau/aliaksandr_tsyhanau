package pages.cloud;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginCloudCloudPage extends CloudPageSwitcher {

    public static final String MAIL_RU_URL = "https://cloud.mail.ru";
    @FindBy(xpath = "//input[@class='nav-inner-col-try__bt' and  @type='button']")
    private WebElement buttonToLogIn;
    @FindBy(id = "PH_user-email")
    private WebElement mailNameField;
    private String loginPageHandle;
    @FindBy(id = "ph_login")
    private WebElement loginInput;
    @FindBy(id = "ph_password")
    private WebElement passwordInput;
    @FindBy(xpath = "//input[@class='x-ph__button__input']")
    private WebElement submitButton;

    public LoginCloudCloudPage(WebDriver driver) {
        super(driver);
    }

    public void openPage() {

        driver.get(MAIL_RU_URL);
        buttonToLogIn.click();
    }

    public MainCloudCloudPage logIn(String login, String password) {
        loginToMailRu(login, password);
        return new MainCloudCloudPage(driver);
    }

    public void loginToMailRu(String login, String password) {
        new WebDriverWait(driver, TEN)
                .until(ExpectedConditions.presenceOfElementLocated(By.id("ph_login")));
        wait.until(ExpectedConditions.visibilityOf(passwordInput));
        loginInput.clear();
        loginInput.sendKeys(login);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        //wait.until(ExpectedConditions.visibilityOf(submitButton));
        click(submitButton);
    }
}
