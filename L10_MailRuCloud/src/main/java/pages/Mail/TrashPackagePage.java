package pages.mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class TrashPackagePage extends PageSwitcher {

    private static final String DELETE_BUTTON_ELEMENT = "(//div[@data-name='remove'])[2]";
    private static final String CHECK_BOX_TRASH_LOCATOR =
            "//*[not(contains(@style, 'display: none'))]//div[contains(@class, 'b-datalist__item__subj')"
                    + " and text()='%s']/ancestor::a//div[contains(@class, 'js-item-checkbox')]"
                    + "//div[@class='b-checkbox__box']";
    private static final String LETTER_SUBJECT_TRASH_LOCATOR =
            "//div[contains(@class, 'b-datalist__item__subj')  and text()='%s']";
    @FindBy(xpath = DELETE_BUTTON_ELEMENT)
    WebElement deleteButton;

    public TrashPackagePage(WebDriver driver) {
        super(driver);
    }

    public TrashPackagePage clickDeleteLastLetter(String subject) {
        for (WebElement element : driver.findElements(By.xpath(String.format(CHECK_BOX_TRASH_LOCATOR, subject)))) {
            wait.until(ExpectedConditions.elementToBeClickable(element));
            element.click();
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(DELETE_BUTTON_ELEMENT)));
        deleteButton.click();
        return this;
    }

    public boolean getLastTrashLetterSubject(String subject) {
        return driver.findElements(By.xpath(String.format(
                LETTER_SUBJECT_TRASH_LOCATOR, subject))).size() > 0 ? true : false;
    }
}
