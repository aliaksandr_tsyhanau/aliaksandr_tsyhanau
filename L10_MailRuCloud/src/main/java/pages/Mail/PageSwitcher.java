package pages.mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.AbstractPage;

public class PageSwitcher extends AbstractPage {

    private static final String SEND_PACKAGE_LOCATOR = "//a[@href='/messages/sent/']//span[@class='b-nav__item__text']";
    private static final String COMPOSE_BUTTON_LOCATOR = "//a[@data-name='compose']";
    private static final String TRASH_PACKAGE_LOCATOR = "(//span[@class='b-nav__item__text'])[4]";
    private static final String LAST_LETTER_LOCATOR = "(//div[@class='b-datalist__item__subj'])[1]";
    private static final String LAST_LETTER_CHECKBOX_LOCATOR = "(//div[@class='b-checkbox__box'])[1]";

    @FindBy(xpath = COMPOSE_BUTTON_LOCATOR)
    WebElement composeButton;
    @FindBy(xpath = SEND_PACKAGE_LOCATOR)
    WebElement sendPackage;
    @FindBy(xpath = TRASH_PACKAGE_LOCATOR)
    WebElement trashPackage;
    @FindBy(xpath = LAST_LETTER_LOCATOR)
    WebElement lastLetter;

    public PageSwitcher(WebDriver driver) {
        super(driver);
    }

    public void clickSendButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(SEND_PACKAGE_LOCATOR)));
        sendPackage.click();
    }

    public void clickTrashButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(TRASH_PACKAGE_LOCATOR)));
        trashPackage.click();
    }

    public void clickComposeButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(COMPOSE_BUTTON_LOCATOR)));
        composeButton.click();
    }

    public String getLastLetterSubject() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LAST_LETTER_CHECKBOX_LOCATOR)));
        return lastLetter.getText();
    }
}
