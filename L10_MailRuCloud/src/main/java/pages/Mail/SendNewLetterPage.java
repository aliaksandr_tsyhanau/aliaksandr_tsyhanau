package pages.mail;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.AbstractPage;

public class SendNewLetterPage extends AbstractPage {

    private static final String INBOX_PACKAGE_LOCATOR =
            "//span[@class='b-nav__item__text b-nav__item__text_unread']";
    @FindBy(xpath = "//input[@id='compose_to']//following-sibling::*[3]")
    WebElement letterAddress;
    @FindBy(xpath = "//input[@name=\"Subject\"]")
    WebElement letterSubject;
    @FindBy(id = "tinymce")
    WebElement letterBody;
    @FindBy(xpath = "//div[@data-name=\"send\"]//span")
    WebElement letterSendButton;
    @FindBy(xpath = "(//*[@id='MailRuConfirm']//button[@class=\"btn btn_stylish btn_main confirm-ok\"])[3]")
    WebElement letterContinueButton;
    @FindBy(xpath = "//div[@id='b-compose__sent']//a[@href='/messages/inbox/']")
    WebElement inboxAlertButton;
    @FindBy(xpath = INBOX_PACKAGE_LOCATOR)
    WebElement inboxPackage;

    public SendNewLetterPage(WebDriver driver) {
        super(driver);
    }

    public void clickInboxButton() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(INBOX_PACKAGE_LOCATOR)));
        if (inboxAlertButton.isDisplayed()) {
            inboxAlertButton.click();
        } else {
            inboxPackage.click();
        }
    }

    public void sendLetter(String lettersAddress, String lettersSubject, String lettersText) {
        letterAddress.sendKeys(lettersAddress);
        letterSubject.sendKeys(lettersSubject);
        driver.switchTo().frame(0);
        letterBody.sendKeys(lettersText);
        driver.switchTo().defaultContent();
        letterSendButton.click();
    }

    public String sendLetter() {
        sendLetter("", "", "");
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        return alertText;
    }

    public void sendLetter(String lettersAddress) {
        sendLetter(lettersAddress, "", "");
        letterContinueButton.click();
    }
}
