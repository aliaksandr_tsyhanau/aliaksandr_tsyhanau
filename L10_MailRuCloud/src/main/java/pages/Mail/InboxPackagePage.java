package pages.mail;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class InboxPackagePage extends PageSwitcher {

    private static final String CHECK_BOX_TRASH_LOCATOR =
            "//div[contains(@class, 'b-datalist__item__subj') and "
                    + "text()='%s']/ancestor::a//div[contains(@class, 'js-item-checkbox')]";
    private static final String DELETE_BUTTON_LOCATOR = "(//div[@data-name='remove'])[1]";
    private static final String EMAIL_LABEL_LOCATOR = "//*[@id='PH_user-email']";
    @FindBy(xpath = DELETE_BUTTON_LOCATOR)
    WebElement deleteButton;
    @FindBy(xpath = EMAIL_LABEL_LOCATOR)
    WebElement emailLabel;

    public InboxPackagePage(WebDriver driver) {
        super(driver);
    }

    public String getEmailText() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(EMAIL_LABEL_LOCATOR)));
        return emailLabel.getText();
    }

    public InboxPackagePage clickDeleteLastLetter(String subject) {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(
                String.format(CHECK_BOX_TRASH_LOCATOR, subject))));
        for (WebElement element : driver.findElements(By.xpath(String.format(CHECK_BOX_TRASH_LOCATOR, subject)))) {
            wait.until(ExpectedConditions.visibilityOf(element));
            element.click();
        }
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(DELETE_BUTTON_LOCATOR)));
        deleteButton.click();
        return this;
    }
}
