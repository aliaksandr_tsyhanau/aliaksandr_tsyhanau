package tests.cloudTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.cloud.OpenedLinkCloudPage;
import pages.cloud.TargetFolderCloudPage;
import pages.cloud.LoginCloudCloudPage;
import pages.cloud.MainCloudCloudPage;
import tests.BeforeAfterTest;

public class CloudTest extends BeforeAfterTest {

    private static final String NEW_FOLDER_NAME = "MyNewFolder";
    private static final String FILE_NAME = "TestDownloadFile";
    private static final String PATH_TO_FILE = "C:/EPAM/TestDownloadFile.txt";
    private static final String PATH_TO_ELEMENTS_ON_MAINPAGE = "//div[contains(@class, 'b-collection__item_datalist')]";
    private static final String PATH_TO_WAITELEMENT_IN_TARGETFOLDER = "//div[@id='breadcrumbs']";

    @Test(description = "Correct input of login and password opens mail.ru cloud.")
    public void positiveLoginTest() {
        LoginCloudCloudPage loginCloudPage = new LoginCloudCloudPage(driver);
        loginCloudPage.openPage();
        MainCloudCloudPage mainCloudPage = loginCloudPage.logIn(CORRECT_NAME, CORRECT_PASS);
        Assert.assertEquals(mainCloudPage.getEmailText(), "lenovot410s@mail.ru", "Login error. Check that "
                + "login and password are correct.");
    }

    @Test(dependsOnMethods = "positiveLoginTest", description = "Creating of new folder.")
    public void createNewFolderTest() {
        MainCloudCloudPage mainCloudPage = new MainCloudCloudPage(driver);
        mainCloudPage.createNewFolder(NEW_FOLDER_NAME);
        Assert.assertTrue(mainCloudPage.checkThatFolderOrFileIsCreated(NEW_FOLDER_NAME, PATH_TO_ELEMENTS_ON_MAINPAGE),
                "Folder " + NEW_FOLDER_NAME + " is not created.");
    }

    @Test(dependsOnMethods = "createNewFolderTest", description = "Check, that new folder was created.")
    public void checkThatNewFolderIsDeletedTest() {
        MainCloudCloudPage mainCloudPage = new MainCloudCloudPage(driver);
        mainCloudPage.deleteFolder(NEW_FOLDER_NAME);
        Assert.assertFalse(mainCloudPage.checkThatFolderOrFileIsCreated(NEW_FOLDER_NAME, PATH_TO_ELEMENTS_ON_MAINPAGE),
                "Folder " + NEW_FOLDER_NAME + " is not removed.");
    }

    @Test(dependsOnMethods = "positiveLoginTest",
            description = "Upload new file to root of cloud and check this handling.")
    public void uploadFileTest() {
        MainCloudCloudPage mainCloudPage = new MainCloudCloudPage(driver);
        mainCloudPage.uploadFile(PATH_TO_FILE, FILE_NAME);
        Assert.assertTrue(mainCloudPage.checkThatFolderOrFileIsCreated(FILE_NAME, PATH_TO_ELEMENTS_ON_MAINPAGE),
                "File " + FILE_NAME + " is not created.");
    }

    @Test(dependsOnMethods = "uploadFileTest", description = "Drug and drop uploaded file to folder."
            + " Check, that this actions was succesfull.")
    public void dragAndDropFileTest() {
        MainCloudCloudPage mainCloudPage = new MainCloudCloudPage(driver);
        mainCloudPage.dragAndDropFile(FILE_NAME);

        TargetFolderCloudPage targetFolderPage = mainCloudPage.openTargetFolder();
        Assert.assertTrue(targetFolderPage.checkThatFolderOrFileIsCreated(FILE_NAME, PATH_TO_WAITELEMENT_IN_TARGETFOLDER),
                "File " + FILE_NAME + " is not moved to folder.");
    }

    @Test(dependsOnMethods = "dragAndDropFileTest",
            description = "Get link of uploaded file, open it and check,that correct file was opened.")
    public void getLinkForUploadedFileTest() {
        TargetFolderCloudPage targetFolderPage = new TargetFolderCloudPage(driver);
        OpenedLinkCloudPage openedLinkPage = targetFolderPage.openLinkOfUploadedFile(FILE_NAME);

        Assert.assertEquals(openedLinkPage.verifyThatLinkOpensFile(FILE_NAME), FILE_NAME + ".txt");
    }
}
