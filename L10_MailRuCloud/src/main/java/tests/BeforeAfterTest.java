package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import java.util.concurrent.TimeUnit;

import static pages.AbstractPage.SIXTY;

public class BeforeAfterTest {

    public static final String CORRECT_NAME = "lenovot410s";
    public static final String CORRECT_PASS = "ovonel";
    public static final String CORRECT_ADDRESS = "lenovot410s@mail.ru";
    public static final String CORRECT_SUBJECT = "lenovo";
    public static final String CORRECT_LETTERSTEXT = "Hello Lenovo!";
    protected WebDriver driver;

    @BeforeClass(description =
            "Create Chromedriver, open it, get maximal size of window, define timeouts, open Login page.")
    public void getDriver() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(SIXTY, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(SIXTY, TimeUnit.SECONDS);
    }

    @AfterClass(description = "Close browser (all opened pages) and quit.")
    public void quitBrowser() {
        driver.quit();
    }
}






