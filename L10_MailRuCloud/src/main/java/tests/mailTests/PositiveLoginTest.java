package tests.mailTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.mail.InboxPackagePage;
import pages.mail.LoginPage;
import tests.BeforeAfterTest;

import static tests.BeforeAfterTest.CORRECT_NAME;

public class PositiveLoginTest extends BeforeAfterTest {

    @Test(description = "Correct input of login and password opens mail box.")
    public void positiveLoginTest() {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage();
        InboxPackagePage inboxPackagePage = loginPage.logIn(CORRECT_NAME, CORRECT_PASS);
        Assert.assertEquals(inboxPackagePage.getEmailText(), "lenovot410s@mail.ru", "Mailbox must "
                + "be opened. Login and password are correct.");
    }
}
