package tests.mailTests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.mail.LoginPage;
import tests.BeforeAfterTest;

public class NegativeLoginTest extends BeforeAfterTest {

    @Test(description = "Incorrect input of login and password opens warning message.")
    public void negativeLoginTest() {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage();
        loginPage.errorLogIn("incorrect", "in12555");
        Assert.assertEquals(loginPage.getIncorrectLoginMessageText(), "Неверное имя или пароль",
                "Mailbox can't be opened. Login and password are incorrect.");
    }
}

