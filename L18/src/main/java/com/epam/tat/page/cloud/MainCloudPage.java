package com.epam.tat.page.cloud;

import com.epam.tat.bo.ItemInCloud;
import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class MainCloudPage extends AbstractPage {

    private static final String CHECK_BOX_FOLDER_LOCATOR =
            "//div[text()='%s']/ancestor::div[contains("
                    + "@class, 'b-thumb__controlbar')]//div[contains(@class, 'b-checkbox__box')]";
    private static final String UPLOADED_FILE_LOCATOR = "//span[text()='%s']";
    private static final String TARGET_FOLDER_LOCATOR =
            "//div[text()='%s']/ancestor::div[contains(@class, 'b-thumb_first')]";
    private static final String UPLOADED_FILE_ICON_LOCATOR =
            "//span[text()='%s']/ancestor::div[contains(@class, 'b-thumb b-thumb_datalist')]";
    private static final String NAME_OF_FOLDER_LOCATOR = "//span[@class='b-filename__name' and text()='%s']";
    private static final By UPLOAD_FILE_ALERT_BUTTON = By.xpath(
            "//input[@type='file' and @class='layer_upload__controls__input']");
    private static final By UPLOAD_FILE_BUTTON = By.xpath("//div[@id='toolbar-left']//div[@data-name='upload']");
    private static final By DELETE_BUTTON = By.xpath("(//div[@data-name='remove'])[1]");
    private static final By EMAIL_LABEL = By.xpath("//*[@id='PH_user-email']");
    private static final By CREATE_NEW_SELECT_BUTTON = By.xpath("//div[@id='toolbar-left']//div[@data-mnemo='ctrl']");
    private static final By CHOOSE_FOLDER_IN_SELECT_BUTTON = By.xpath(
            "//*[@id='toolbar-left']//a[@data-name='folder']/span");
    private static final By NEW_FOLDER_NAME_INPUT = By.xpath("//input[@class='layer__input']");
    private static final By NEW_FOLDER_ADD_BUTTON = By.xpath("//button[@data-name='add']");
    private static final By ALERT_DELETE_BUTTON = By.xpath("//button[@data-name='remove']");
    private static final By CONFIRM_BUTTON = By.xpath("(//button[@data-name='close'])[7]");
    private static final By MOVE_CONFIRM_BUTTON = By.xpath("(//button[@data-name='move'])[1]");
    private static final By GET_LINK_BUTTON = By.xpath("//a[@data-name='publish']");
    private static final By OPEN_LINK_BUTTON = By.xpath("//button[@data-name='popup']");
    private static final By ALL_FOLDERS_AND_FILES = By.xpath("//div[contains(@class, 'b-collection__item_datalist')]");
    private static final String PATH_TO_ELEMENTS_ON_MAINPAGE = "//div[contains(@class, 'b-collection__item_datalist')]";
    private static final String PATH_TO_WAITELEMENT_IN_TARGETFOLDER = "//div[@id='breadcrumbs']";

    public MainCloudPage() throws MalformedURLException {
        super();
    }

    public String getEmailText() {
        browser.waitForElementIsVisible(EMAIL_LABEL);
        return browser.getText(EMAIL_LABEL);
    }

    public MainCloudPage clickCreateNewSelectButton() {
        browser.click(CREATE_NEW_SELECT_BUTTON);
        return this;
    }

    public MainCloudPage clickChooseFolderInSelectButton() {
        browser.waitForElementIsVisible(CHOOSE_FOLDER_IN_SELECT_BUTTON);
        browser.click(CHOOSE_FOLDER_IN_SELECT_BUTTON);
        return this;
    }

    public MainCloudPage inputNameOfNewFolder(String folderName) {
        browser.waitForElementIsVisible(NEW_FOLDER_NAME_INPUT);
        browser.sendKeys(NEW_FOLDER_NAME_INPUT, folderName);
        return this;
    }

    public MainCloudPage clickAddButton() {
        browser.waitForElementIsVisible(NEW_FOLDER_NAME_INPUT);
        browser.click(NEW_FOLDER_ADD_BUTTON);
        return this;
    }

    public boolean checkItemInRoot(String newFolderName) {
        return browser.checkItemInRoot(newFolderName, PATH_TO_ELEMENTS_ON_MAINPAGE, ALL_FOLDERS_AND_FILES);
    }

    public boolean checkItemInFolder(String newFileName) {
        return browser.checkItemInRoot(newFileName, PATH_TO_WAITELEMENT_IN_TARGETFOLDER, ALL_FOLDERS_AND_FILES);
    }

    public MainCloudPage clickCheckbox(ItemInCloud itemInCloud) {
        By checkboxLocator = By.xpath(String.format(
                CHECK_BOX_FOLDER_LOCATOR, itemInCloud.getName()));
        browser.waitForElementIsVisible(checkboxLocator);
        browser.click(checkboxLocator);
        return this;
    }

    public MainCloudPage clickDeleteButton() {
        browser.waitForElementIsVisible(DELETE_BUTTON);
        browser.click(DELETE_BUTTON);
        return this;
    }

    public MainCloudPage clickAlertButton() {
        browser.waitForElementIsVisible(ALERT_DELETE_BUTTON);
        browser.click(ALERT_DELETE_BUTTON);
        return this;
    }

    public MainCloudPage clickConfirmButton() {
        if (browser.isDisplayed(CONFIRM_BUTTON)) {
            browser.click(CONFIRM_BUTTON);
        }
        return this;
    }

    public MainCloudPage clickUploadFileButton() {
        browser.click(UPLOAD_FILE_BUTTON);
        return this;
    }

    public MainCloudPage chooseAndUploadFile(ItemInCloud itemInCloud) {
        browser.sendKeys(UPLOAD_FILE_ALERT_BUTTON, itemInCloud.getPathToItem());
        browser.waitForElementIsVisible(By.xpath(String.format(UPLOADED_FILE_LOCATOR, itemInCloud.getName())));
        return this;
    }

    public MainCloudPage dragAndDropFile(ItemInCloud itemInCloud, ItemInCloud itemInCloudFolder) {
        By targetFolderLocator = By.xpath(String.format(TARGET_FOLDER_LOCATOR, itemInCloudFolder.getName()));
        By draggable = By.xpath(String.format(UPLOADED_FILE_ICON_LOCATOR, itemInCloud.getName()));
        browser.dragAndDropFile(draggable, targetFolderLocator);
        return this;
    }

    public MainCloudPage clickMoveConfirmButton() {
        browser.waitForElementIsVisible(MOVE_CONFIRM_BUTTON);
        browser.click(MOVE_CONFIRM_BUTTON);
        return this;
    }

    public OpenedLinkCloudPage openLinkOfUploadedFile(String fileName) throws MalformedURLException {
        By uploadedFile = By.xpath(String.format(UPLOADED_FILE_ICON_LOCATOR, fileName));
        browser.rightMouseClick(uploadedFile, GET_LINK_BUTTON);
        browser.click(OPEN_LINK_BUTTON);
        browser.switchToNewWindow();
        return new OpenedLinkCloudPage();
    }
}
