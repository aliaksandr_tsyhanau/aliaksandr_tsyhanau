package com.epam.tat.page.mail;

import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;

public class PageSwitcher extends AbstractPage {

    private static final By INBOX_PACKAGE = By.xpath(
            "//a[@href='/messages/inbox/']//span[contains(@class, 'b-nav__item__text')]");
    private static final By COMPOSE_BUTTON = By.xpath("(//a[@data-name='compose'])[1]");
    private static final By SEND_PACKAGE = By.xpath("//a[@href='/messages/sent/']");
    private static final By TRASH_PACKAGE = By.xpath("(//span[@class='b-nav__item__text'])[4]");
    private static final By LAST_LETTER_CHECKBOX = By.xpath("(//div[@class='b-checkbox__box'])[1]");
    private static final By LAST_LETTER = By.xpath("//div[contains(@data-cache-key, 'undefined_false') and "
            + "not(contains(@style, 'display: none'))]//div[@class='b-datalist__item__subj']");

    public PageSwitcher() throws MalformedURLException {
        super();
    }

    public void clickSendButton() {
        browser.waitForElementIsVisible(SEND_PACKAGE);
        browser.click(SEND_PACKAGE);
    }

    public void clickTrashButton() {
        browser.waitForElementIsVisible(TRASH_PACKAGE);
        browser.click(TRASH_PACKAGE);
    }

    public void clickComposeButton() {
        browser.waitForElementIsVisible(COMPOSE_BUTTON);
        browser.click(COMPOSE_BUTTON);
    }

    public void clickInboxBtn() {
        browser.click(INBOX_PACKAGE);
    }

    public boolean checkThatLetterHasSubject(String subject) {

        WebElement lastLetter = browser.findElement(By.xpath(String.format("//*[contains(text(), '%s')]", subject)));
        try {
            if (browser.isDisplayed(lastLetter)) {
                return true;
            }
        } catch (NotFoundException e) {
            System.out.println("is not displayd " + e);
        }
        return false;

        //return  browser.checkLetterHasSubject(LAST_LETTER, subject);
        //        List<WebElement> letters = browser.findElements(locator);
        //        for (WebElement letter : letters) {
        //            if (browser.getText(letter) == null) {
        //                letters.remove(letter);
        //                System.out.println("Size of list with letters is " + letters.size());
        //            }
        //        }
        //
        //        int i = 1;
        //        for (WebElement letter : letters) {
        //            System.out.println("I am visible letter Nr." + i + " >>> " + letter.getText());
        //            i++;
        //        }
        //        System.out.println("There are " + letters.size() + " visible letters in this box.");
        //
        //        for (WebElement letter : letters) {
        //            System.out.println(browser.getText(letter) + "___"  + subject);
        //            if (browser.getText(letter).equals(subject)) {
        //                return true;
        //            }
        //        }
        //        return false;
    }
}
