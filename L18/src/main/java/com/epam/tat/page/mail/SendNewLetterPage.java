package com.epam.tat.page.mail;

import com.epam.tat.logs.Log;
import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.*;

import java.net.MalformedURLException;

public class SendNewLetterPage extends AbstractPage {

    private static final By INBOX_PACKAGE = By.xpath(
            "//a[@href='/messages/inbox/' and @class='b-nav__link']");
    // "//a[@href='/messages/inbox/']//span[contains(@class, 'b-nav__item__text')]");
    private static final By LETTER_ADDRESS = By.xpath("//input[@id='compose_to']//following-sibling::*[3]");
    private static final By LETTER_SUBJECT = By.xpath("//input[@name='Subject']");
    private static final By LETTER_BODY = By.xpath("//*[@id='tinymce']");
    private static final By LETTER_SEND_BUTTON = By.xpath("//div[@data-name='send']//span");
    private static final By LETTER_CONTINUE_BUTTON =
            By.xpath("(//*[@id='MailRuConfirm']//button[@class=\"btn btn_stylish btn_main confirm-ok\"])[3]");
    private static final By INBOX_ALERT_BUTTON = By.xpath("//div[@id='b-compose__sent']//a[@href='/messages/inbox/']");
    private static final By BANNER_LOCATOR = By.xpath("//div[@class='rb_banner']");

    public SendNewLetterPage() throws MalformedURLException {
        super();
    }

    public InboxPackagePage clickInboxButton() throws MalformedURLException {
        WebElement b = browser.findElement(BANNER_LOCATOR);
        ((JavascriptExecutor) browser.getWrappedDriver()).executeScript(
                "arguments[0].style.display='none'", browser.findElement(BANNER_LOCATOR));

        browser.waitForElementIsVisible(INBOX_PACKAGE);
        //browser.waitForElementIsVisible(INBOX_ALERT_BUTTON);
        //browser.getWrappedDriver().wait(ExpectedConditions.elementToBeClickable(By.xpath(INBOX_PACKAGE));
        Log.debug("Click Inbox_Page Button-----------------------------------------------------");
        browser.click(INBOX_PACKAGE);
        return new InboxPackagePage();

    }

    public SendNewLetterPage fillLettersAddress(String lettersAddress) {
        browser.sendKeys(LETTER_ADDRESS, lettersAddress);
        return this;
    }

    public SendNewLetterPage fillLettersSubject(String lettersSubject) {
        browser.sendKeys(LETTER_SUBJECT, lettersSubject);
        return this;
    }

    public SendNewLetterPage fillLettersText(String lettersText) {
        browser.switchToFrame(0);
        browser.sendKeys(LETTER_BODY, lettersText);
        return this;
    }

    public SendNewLetterPage clickSendButton() {
        browser.switchToDefaultContent();
        browser.click(LETTER_SEND_BUTTON);
        return this;
    }

    public void clickContinueButton() {
        browser.click(LETTER_CONTINUE_BUTTON);
    }

    public String getTextFromAlert() {
        return browser.getTextFromAlert();
    }
}
