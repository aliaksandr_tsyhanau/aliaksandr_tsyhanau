package com.epam.tat.page.mail;

import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class InboxPackagePage extends PageSwitcher {

    private static final String CHECK_BOX_TRASH_LOCATOR =
            "//div[contains(@class, 'b-datalist__item__subj') and text()='%s']/ancestor::a//div[contains(@class,"
                    + " 'js-item-checkbox')]//div[@class='b-checkbox__box']";
    private static final By DELETE_BUTTON = By.xpath("//div[contains(@data-cache-key, 'undefined_false') and "
            + "not(contains(@style, 'display: none'))]//div[@data-name='remove']");

    private static final By EMAIL_LABEL = By.xpath("//*[@id='PH_user-email']");

    public InboxPackagePage() throws MalformedURLException {
        super();
    }

    public String getEmailText() {
        browser.waitForElementIsVisible(EMAIL_LABEL);
        return browser.getText(EMAIL_LABEL);
    }

    public InboxPackagePage clickDeleteLastLetter(String subject) {
        browser.waitForElementIsVisible(By.xpath(
                String.format(CHECK_BOX_TRASH_LOCATOR, subject)));
        By locator = By.xpath(String.format(CHECK_BOX_TRASH_LOCATOR, subject));

        browser.waitForElementIsVisible(DELETE_BUTTON);
        browser.click(locator);

        browser.click(DELETE_BUTTON);
        return this;
    }
}
