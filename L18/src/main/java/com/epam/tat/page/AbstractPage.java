package com.epam.tat.page;

import com.epam.tat.framework.ui.Browser;
import java.net.MalformedURLException;

public class AbstractPage {

    protected Browser browser;

    public AbstractPage() throws MalformedURLException {
        this.browser = Browser.getBrowser();
    }
}

