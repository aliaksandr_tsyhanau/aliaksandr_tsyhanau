package com.epam.tat.test.mail;

import com.epam.tat.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

import static com.epam.tat.bo.LetterFactory.getIncorrectLetterWithoutAddress;

public class SendMailWithoutAddressTest extends BeforeSendTest {

    private static  final String EXPECTED_ERROR_MESSAGE = "Не указан адрес получателя";

    @Test(description = "Sending letter  without address will throw exeption.")
    public void sendMailWithoutAddressTest() throws MalformedURLException {

        String errorMessage = MailService.sendNewLetterWithoutAddress(getIncorrectLetterWithoutAddress());
        System.out.println(errorMessage);

        Assert.assertEquals(errorMessage, EXPECTED_ERROR_MESSAGE, "Message in alert is not correct.");
    }
}
