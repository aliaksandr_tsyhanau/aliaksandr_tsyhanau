package com.epam.tat.test.mail;

import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class SendAndDeleteMailTest extends BeforeSendTest {

    Letter letter = LetterFactory.getCorrectLetter();

    @Test(description = "After sending letter to correct "
            + "address, and deleting it we will see this letter in Trash folder.")
    public void sendMailTest() throws MalformedURLException {

        MailService.sendNewLetter(letter);
        MailService.clickInboxButton();

        MailService.deleteLastLetter(letter);
        MailService.moveToTrash();
        Assert.assertTrue(MailService.getLastTrashLetterSubject(letter),
                "Letter is not in draft or subject is incorrect");
    }

    @Test(dependsOnMethods = "sendMailTest",
            description = "Delete letter from Trash folder and check that it is not present in Trash.")
    public void deleteMailTest() throws MalformedURLException {

        //Letter letter = new Letter();
        MailService.deleteLastLetter(letter);
        Assert.assertFalse(MailService.getLastTrashLetterSubject(letter),
                "This letter must be deleted from draft");
        //        Assert.assertEquals(trashPackagePage.getLastLetterSubject(), DRAFT_SUBJECT + DRAFT_TEXT,
        //                "This letter must be deleted from draft");
    }
}
