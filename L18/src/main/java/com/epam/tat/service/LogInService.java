package com.epam.tat.service;

import com.epam.tat.bo.Account;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.logs.Log;
import com.epam.tat.page.cloud.LoginCloudPage;
import com.epam.tat.page.mail.LoginPage;

import java.net.MalformedURLException;

public class LogInService {

    public static void logInMailBox(Account account) throws MalformedURLException {
        Log.info("Log in MAIL.RU");
        new LoginPage()
                .openPage()
                .typeLogin(account.getName())
                .typePassword(account.getPassword())
                .clickSubmitLogIn();
    }

    public static void logInCloud(Account account) throws MalformedURLException {
        Log.info("Log in CLOUD.MAIL.RU with login " + account.getName() + " and password " + account.getPassword());
        new LoginCloudPage()
                .openPage()
                .typeLogin(account.getName())
                .typePassword(account.getPassword())
                .clickSubmitButton();
    }

    public static void quitBrowser() throws MalformedURLException {
        Log.info("Close Browser.");
        Browser.getBrowser().close();
    }
}
