package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    public static final int SIXTY = 60;

    public static WebDriver getWebDriver() throws MalformedURLException {

        WebDriver driver;
        DesiredCapabilities capabilities = new DesiredCapabilities();

        switch (Parameters.instance().getBrowserType()) {
            case FIREFOX:
                //capabilities.setBrowserName("firefox");
                capabilities.setBrowserName(BrowserType.FIREFOX);
                //capabilities = DesiredCapabilities.firefox();
                //System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                capabilities.setPlatform(Platform.WINDOWS);
                driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
                break;
            case CHROME:
                //System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                //System.setProperty("webdriver.chrome.driver", "C:\\EPAM\\HometasksTAT2017\\L18\\src\\main\\
                // resources\\chromedriver.exe");
                //                capabilities = DesiredCapabilities.chrome();
                //                capabilities.setBrowserName("chrome");
                //                capabilities.setPlatform(org.openqa.selenium.Platform.WINDOWS);
                //                driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
                capabilities = DesiredCapabilities.chrome();

                driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(SIXTY, TimeUnit.SECONDS);
                driver.manage().timeouts().pageLoadTimeout(SIXTY, TimeUnit.SECONDS);
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.instance().getBrowserType());
        }

        return driver;
    }
}
