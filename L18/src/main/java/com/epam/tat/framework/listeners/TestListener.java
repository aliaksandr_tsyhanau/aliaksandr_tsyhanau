package com.epam.tat.framework.listeners;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.logs.Log;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.net.MalformedURLException;

public class TestListener extends TestListenerAdapter {

    public void onStart(ITestContext iTestContext) {

        // onStart(iTestContext);
        Log.info("[TEST STARTED]" + iTestContext.getName());

    }

    public void onFinish(ITestContext iTestContext) {

        //onFinish(iTestContext);
        Log.info("[TEST FINISHED]" + iTestContext.getName());
        //        Log.debug("Quits this driver, closes all windows.");
        try {
            Browser.getBrowser().close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    public void onTestFailure(ITestResult tr) {
        //onTestFailure(tr);
        try {
            Browser.getBrowser().screenshot();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        Log.error("Test is failed");
    }
}
