package com.epam.tat.framework.listeners;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.logs.Log;
import org.testng.ISuite;
import org.testng.ISuiteListener;

import java.net.MalformedURLException;

public class SuiteListener implements ISuiteListener {

    //public static final int THREAD_COUNT = 2;

    @Override
    public void onStart(ISuite iSuite) {
        //iSuite.getXmlSuite().setParallel(XmlSuite.ParallelMode.CLASSES);
        //iSuite.getXmlSuite().setThreadCount(THREAD_COUNT);
        Log.info("Suite started  " + iSuite.getName());
    }

    @Override
    public void onFinish(ISuite iSuite) {
        Log.info("Suite finished  " + iSuite.getName());
        try {
            Browser.getBrowser().close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
