package com.epam.tat.framework.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomUtils {

    private static final int FIVE = 5;

    public static String getRandomString() {
        return RandomStringUtils.randomAlphabetic(FIVE);
    }
}
