package com.epam.tat.bo;

import static com.epam.tat.framework.utils.RandomUtils.getRandomString;

public class AccountFactory {

    private static final String CORRECT_NAME = "lenovot410s";
    private static final String CORRECT_PASSWORD = "ovonel";

    public static Account getExistentAccount() {
        return new AccountBuilder()
                 .name(CORRECT_NAME)
                 .password(CORRECT_PASSWORD)
                 .build();
                //        Account account = new Account();
                //        account.setName(CORRECT_NAME);
                //        account.setPassword(CORRECT_PASSWORD);
                //        return account;
    }

    public static Account getInvalidAccount() {
        Account account = new Account();
        account.setName(getRandomString());
        account.setPassword(getRandomString());
        return account;
    }
}
