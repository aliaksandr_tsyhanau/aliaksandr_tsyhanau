package com.epam.tat.bo;

public class ItemInCloud {
    private String name;
    private String pathToItem;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathToItem() {
        return pathToItem;
    }

    public void setPathToItem(String pathToItem) {
        this.pathToItem = pathToItem;
    }
}
