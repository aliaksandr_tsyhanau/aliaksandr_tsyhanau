package com.epam.tat;

import java.util.*;

/**
 * Created by LENOVO on 13.07.2017.
 */
public class VerificationOfShapesUnicity {
    private String s;
    private Set<String> verifySet = new HashSet<>();

    public void verifyUnicity(String str) {
        s = str;
        for (String elem : verifySet) {
            if (elem.equals(s)) {
                System.out.println("This value <" + s + "> is not unique. Duplication is not allowed!");
            }
        }
        verifySet.add(s);
    }

}
