package com.epam.tat;

/**
 * Created by LENOVO on 15.07.2017.
 */
public class Triangle implements Shape {

    static final int THREE = 3;
    static final int FOUR = 4;

    private int a;
    private String name;

    public Triangle(String s, int i) {
        name = s;
        a = i;
    }

    public String getName() {
        return name;
    }

    @Override
    public int area() {
        return (int) (Math.sqrt(THREE) * Math.pow(a, 2) / FOUR);
    }

    @Override
    public int perimeter() {
        return a * THREE;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Triangle)) {
            return false;
        }

        Triangle triangle = (Triangle) o;

        return a == triangle.a;
    }

    @Override
    public int hashCode() {
        return a * area();
    }
}
