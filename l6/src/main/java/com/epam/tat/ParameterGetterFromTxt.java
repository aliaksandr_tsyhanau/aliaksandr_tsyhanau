package com.epam.tat;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by LENOVO on 11.07.2017.
 */
public class ParameterGetterFromTxt {
    public static String[] getParameters(String pathToTxt) throws IOException {
        try {
            File fileTxt = new File(pathToTxt);
            List lines = FileUtils.readLines(fileTxt, "UTF-8");
            String[] param = new String[lines.size()];
            lines.toArray(param);
            return param;
        } catch (IOException ex) {
            System.out.println(ex.toString());
            System.out.println("Could not find file in directory " + pathToTxt);
        }
        return new String[0];
    }
}


