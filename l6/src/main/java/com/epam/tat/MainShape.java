package com.epam.tat;

import java.io.IOException;
import java.util.*;
import java.util.List;

import static com.epam.tat.ParameterGetterFromTxt.getParameters;

/**
 * Created by LENOVO on 04.07.2017.
 */

public class MainShape {
    public static void main(String[] args) {

        List<String> parsedInput = new ArrayList<>(args.length);

        if (args[0].equals("--path")) {
            String b = args[1];
            try {
                for (int i = 0; i < getParameters(b).length; i++) {
                    parsedInput.add(getParameters(b)[i]);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            for (int i = 1; i < args.length; i++) {
                parsedInput.add(args[i]);
            }
        }

        Set<Shape> unicitysSet = new HashSet<>();
        unicitysSet = setShapesToSet(parsedInput);

        printResult(unicitysSet);

    }

    public static Set<Shape> setShapesToSet(List<String> arrLis) {
        Set<Shape> unicitysSet = new HashSet<>();
        List<String> parsedShape = new ArrayList<>();
        for (String elem : arrLis) {
            parsedShape = Arrays.asList(elem.split(":"));
            switch (ShapeTypes.valueOf(parsedShape.get(0).toUpperCase())) {
                case TRIANGLE:
                    unicitysSet.add(new Triangle(elem, Integer.parseInt(parsedShape.get(1))));
                    break;
                case SQUARE:
                    unicitysSet.add(new Square(elem, Integer.parseInt(parsedShape.get(1))));
                    break;
                case CIRCLE:
                    unicitysSet.add(new Circle(elem, Integer.parseInt(parsedShape.get(1))));
                    break;
                default:
                    unicitysSet.add(new Rectangle(elem, Integer.parseInt(parsedShape.get(1)),
                            Integer.parseInt(parsedShape.get(2))));
                    break;
            }
        }
        return unicitysSet;
    }

    public static void printResult(Set<Shape> set) {
        Map<String, Integer> resultsWithAreaAsValue = new HashMap<>();
        for (Shape el : set) {
            String sss = el.getName() + "\n" + "   area " + el.area() + "\n" + "   perimeter " + el.perimeter();
            resultsWithAreaAsValue.put(sss, el.area());
        }

        MyComparator comp = new MyComparator(resultsWithAreaAsValue);
        Map<String, Integer> newMap = new TreeMap(comp);
        newMap.putAll(resultsWithAreaAsValue);

        for (Map.Entry<String, Integer> entry : newMap.entrySet()) {
            System.out.println(entry.getKey());
        }
    }
}
