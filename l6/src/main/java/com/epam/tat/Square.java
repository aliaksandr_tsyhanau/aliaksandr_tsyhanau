package com.epam.tat;

/**
 * Created by LENOVO on 15.07.2017.
 */
public class Square implements Shape {

    static final int FOUR = 4;

    private int a;
    private String name;

    public Square(String s, int i) {
        name = s;
        a = i;
    }

    public String getName() {
        return name;
    }

    @Override
    public int area() {
        return a * a;
    }

    @Override
    public int perimeter() {
        return a * FOUR;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Square)) {
            return false;
        }

        Square square = (Square) o;

        return a == square.a;
    }

    @Override
    public int hashCode() {
        return a * area();
    }
}
