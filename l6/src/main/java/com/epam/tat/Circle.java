package com.epam.tat;

/**
 * Created by LENOVO on 15.07.2017.
 */
public class Circle implements Shape {

    private int a;
    private String name;

    public Circle(String s, int i) {
        name = s;
        a = i;
    }

    public String getName() {
        return name;
    }

    @Override
    public int area() {
        return (int) (Math.PI * Math.pow(a, 2));
    }

    @Override
    public int perimeter() {
        return (int) (2 * Math.PI * a);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Circle)) {
            return false;
        }

        Circle circle = (Circle) o;

        return a == circle.a;
    }

    @Override
    public int hashCode() {
        return a * area();
    }
}
