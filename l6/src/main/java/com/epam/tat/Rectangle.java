package com.epam.tat;

/**
 * Created by LENOVO on 15.07.2017.
 */
public class Rectangle implements Shape {

    static final int THREETY_ONE = 31;

    private int a;
    private int b;
    private String name;

    public Rectangle(String s, int i, int width) {
        name = s;
        a = i;
        b = width;
    }

    public String getName() {
        return name;
    }

    @Override
    public int area() {
        return a * b;
    }

    @Override
    public int perimeter() {
        return (a + b) * 2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Rectangle)) {
            return false;
        }

        Rectangle rectangle = (Rectangle) o;

        if (a != rectangle.a) {
            return false;
        }
        return b == rectangle.b;
    }

    @Override
    public int hashCode() {
        int result = a;
        result = THREETY_ONE * result + b;
        return result;
    }
}
