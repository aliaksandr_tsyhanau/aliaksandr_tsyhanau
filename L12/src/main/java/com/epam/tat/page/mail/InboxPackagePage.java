package com.epam.tat.page.mail;

import org.openqa.selenium.By;

public class InboxPackagePage extends PageSwitcher {

    private static final String CHECK_BOX_TRASH_LOCATOR =
            "//div[contains(@class, 'b-datalist__item__subj') and "
                    + "text()='%s']/ancestor::a//div[contains(@class, 'js-item-checkbox')]";
    private static final By DELETE_BUTTON = By.xpath("//div[@data-name='remove' and @data-shortcut-title='Del' " +
            "and not(contains(@style,'display:none')) and xpath = '1']");//(//div[@data-name='remove'])[1]
    //<div data-bem="b-toolbar__btn" data-name="remove" data-shortcut="del|command+backspace"  data-title="Удалить (Del)"><i data-bem="ico" class="ico ico_toolbar ico_toolbar_remove"></i><span class="b-toolbar__btn__text b-toolbar__btn__text_pad">Удалить</span></div>
    private static final By EMAIL_LABEL = By.xpath("//*[@id='PH_user-email']");

    public InboxPackagePage() {
        super();
    }

    public String getEmailText() {
        browser.waitForElementIsVisible(EMAIL_LABEL);
        return browser.getText(EMAIL_LABEL);
    }

    public InboxPackagePage clickDeleteLastLetter(String subject) {
        browser.waitForElementIsVisible(By.xpath(
                String.format(CHECK_BOX_TRASH_LOCATOR, subject)));
        By locator = (By.xpath(String.format(CHECK_BOX_TRASH_LOCATOR, subject)));
        browser.click(locator);
        //browser.waitForElementPresent(DELETE_BUTTON);
        browser.clickAllElements(DELETE_BUTTON);
        return this;
    }
}
