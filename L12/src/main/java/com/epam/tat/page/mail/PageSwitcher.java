package com.epam.tat.page.mail;

import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PageSwitcher extends AbstractPage {

    private static final By COMPOSE_BUTTON = By.xpath("//a[@data-name='compose']");
    private static final By SEND_PACKAGE = By.xpath("//span[@class='b-nav__item__text'][contains(text(),'Отправленные')]");
    private static final By TRASH_PACKAGE = By.xpath("(//span[@class='b-nav__item__text'])[4]");
    private static final By LAST_LETTER_CHECKBOX = By.xpath("(//div[@class='b-checkbox__box'])[1]");
    private static final By LAST_LETTER= By.xpath("//div[@class='b-datalist__item__subj']");

    public PageSwitcher() {
        super();
    }

    public void clickSendButton() {
        //browser.customWait(SEND_PACKAGE);
        browser.waitForElementIsVisible(SEND_PACKAGE);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        browser.click(SEND_PACKAGE);
    }

    public void clickTrashButton() {
        browser.waitForElementIsVisible(TRASH_PACKAGE);
        browser.click(TRASH_PACKAGE);
    }

    public void clickComposeButton() {
        browser.waitForElementIsVisible(COMPOSE_BUTTON);
        browser.click(COMPOSE_BUTTON);
    }

    public boolean checkLetterHasSubject(String subject) {
      return  browser.checkLetterHasSubject(LAST_LETTER, subject);


    }
}
