package com.epam.tat.page.mail;

import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;

public class LoginPage extends AbstractPage {

    private static final String MAIL_RU_URL = "https://mail.ru";
    private static final By LOGIN_INPUT = By.xpath("//*[@id='mailbox:login']");
    private static final By PASSWORD_INPUT = By.xpath("//*[@id='mailbox:password']");
    private static final By SUBMIT_BUTTON = By.xpath("//*[@id='mailbox:submit']");
    private static final By INCORRECT_LOGIN_MESSAGE = By.xpath("//*[@id='mailbox:error']");

    public LoginPage() {
        super();
    }

    public LoginPage openPage() {
        browser.open(MAIL_RU_URL);
        return this;
    }

    public LoginPage typeLogin(String login) {
        browser.waitForElementIsVisible(LOGIN_INPUT);
        browser.clear(LOGIN_INPUT);
        browser.sendKeys(LOGIN_INPUT, login);
        return this;
    }

    public LoginPage typePassword(String password) {
        browser.waitForElementIsVisible(PASSWORD_INPUT);
        browser.clear(PASSWORD_INPUT);
        browser.sendKeys(PASSWORD_INPUT, password);
        return this;
    }

    public LoginPage clickSubmitLogIn() {
        browser.waitForElementIsVisible(SUBMIT_BUTTON);
        browser.click(SUBMIT_BUTTON);
        return this;
    }

    public String getIncorrectLoginMessageText() {
        browser.waitForElementIsVisible(INCORRECT_LOGIN_MESSAGE);
        return browser.getText(INCORRECT_LOGIN_MESSAGE);
    }
}
