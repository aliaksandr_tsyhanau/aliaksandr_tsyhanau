package com.epam.tat.page.mail;

import org.openqa.selenium.By;

public class SendPackagePage extends PageSwitcher {

    private static final By LAST_LETTER = By.xpath("(//span[@class='b-datalist__item__subj__snippet'])[1]");
    //span[@class='b-datalist__item__subj__snippet'][contains(text(),'Hello Lenovo!VQSGI'

    public SendPackagePage() {
        super();
    }

    public boolean checkLetterHasSubject(String subject) {
        return browser.checkLetterHasSubject(LAST_LETTER, subject);
    }
}
