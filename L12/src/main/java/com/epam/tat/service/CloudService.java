package com.epam.tat.service;

import com.epam.tat.bo.ItemInCloud;
import com.epam.tat.page.cloud.MainCloudPage;
import com.epam.tat.page.cloud.OpenedLinkCloudPage;

public class CloudService {

    public static void createFolder(ItemInCloud itemInCloud) {
        new MainCloudPage()
                .clickCreateNewSelectButton()
                .clickChooseFolderInSelectButton()
                .inputNameOfNewFolder(itemInCloud.getName())
                .clickAddButton();
    }

    public static boolean checkItemInRoot(ItemInCloud itemInCloud) {
        return new MainCloudPage().checkItemInRoot(itemInCloud.getName());
    }

    public static boolean checkItemInFolder(ItemInCloud itemInCloud) {
        return new MainCloudPage().checkItemInFolder(itemInCloud.getName());
    }

    public static void deleteFolder(ItemInCloud itemInCloud) {
        new MainCloudPage()
                .clickCheckbox(itemInCloud)
                .clickDeleteButton()
                .clickAlertButton()
                .clickConfirmButton();
    }

    public static void uploadFile(ItemInCloud itemInCloud) {
        new MainCloudPage()
                .clickUploadFileButton()
                .chooseAndUploadFile(itemInCloud);
    }

    public static void dragAndDropFile(ItemInCloud itemInCloudFile, ItemInCloud itemInCloudFolder) {
        new MainCloudPage()
                .dragAndDropFile(itemInCloudFile, itemInCloudFolder)
                .clickMoveConfirmButton();
    }

    public static String getNameOfLinkOpensFile(ItemInCloud itemInCloud) {
        new MainCloudPage().openLinkOfUploadedFile(itemInCloud.getName());
        return new OpenedLinkCloudPage().getNameOfLinkOpensFile();
    }
}
