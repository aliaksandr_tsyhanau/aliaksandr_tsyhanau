package com.epam.tat.service;

import com.epam.tat.bo.Account;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.page.cloud.LoginCloudPage;
import com.epam.tat.page.mail.LoginPage;

public class LogInService {

    public static void logInMailBox(Account account) {
        new LoginPage()
                .openPage()
                .typeLogin(account.getName())
                .typePassword(account.getPassword())
                .clickSubmitLogIn();
    }

    public static void logInCloud(Account account) {
        new LoginCloudPage()
                .openPage()
                .typeLogin(account.getName())
                .typePassword(account.getPassword())
                .clickSubmitButton();
    }

    public static void quitBrowser() {
        Browser.getBrowser().close();
    }
}
