package com.epam.tat.service;

import com.epam.tat.bo.Letter;
import com.epam.tat.page.mail.*;

public class MailService {

    public static void sendNewLetter(Letter letter) {

        new InboxPackagePage().clickComposeButton();
        new SendNewLetterPage()
                .fillLettersAddress(letter.getEmail())
                .fillLettersSubject(letter.getSubject())
                .fillLettersText(letter.getBodyText())
                .clickSendButton();

    }

    public static String sendNewLetterWithoutAddress(Letter letter) {
        sendNewLetter(letter);

        return new SendNewLetterPage().getTextFromAlert();
    }

    public static void sendNewLetterWithoutSubjectAndText(Letter letter) {
        sendNewLetter(letter);
        new SendNewLetterPage().clickContinueButton();
    }

    public static void clickInboxButton() {
        new SendNewLetterPage().clickInboxButton();
    }

    public static boolean getLastLetterSubject(String subject) {
        return new PageSwitcher().checkLetterHasSubject(subject);
    }

    public static void moveToSended() {
        new PageSwitcher().clickSendButton();
    }

    public static boolean getLastSendLetterSubject(String subject) {
        return new SendPackagePage().checkLetterHasSubject(subject);
    }

    public static void deleteLastLetter(Letter letter) {
        new InboxPackagePage().clickDeleteLastLetter(letter.getSubject());
    }

    public static void moveToTrash() {
        new InboxPackagePage().clickTrashButton();
    }

    public static boolean getLastTrashLetterSubject(Letter letter) {
        return new TrashPackagePage().getLastTrashLetterSubject(letter.getSubject());
    }

}
