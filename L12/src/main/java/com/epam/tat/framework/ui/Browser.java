package com.epam.tat.framework.ui;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public final class Browser implements WrapsDriver {

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();

    private WebDriver driver;

    private Browser() {
        this.driver = WebDriverFactory.getWebDriver();
    }

    public static synchronized Browser getBrowser() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void close() {
        if (getWrappedDriver() != null) {
            getWrappedDriver().quit();
        }
        instance.remove();
    }

    @Override
    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String url) {
        driver.get(url);
    }

    public WebElement findElement(By by) {
        return driver.findElement(by);
    }

    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    public void click(By locator) {
        waitForElementPresent(locator);
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].style.border='3px solid red'", findElement(locator));
        findElement(locator).click();
/*        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].style.border='0px'", findElement(locator));*/
    }

    private void click(WebElement element) {
        waitForElementIsVisible(element);
        /*((JavascriptExecutor) driver).executeScript(
                "arguments[0].style.border='3px solid red'", element);*/
        element.click();
        /*((JavascriptExecutor) driver).executeScript(
                "arguments[0].style.border='0px'", element);*/
    }

    public void rightMouseClick(By uploadedFile, By getLinkButtonLocator) {
        waitForElementIsVisible(uploadedFile);
        WebElement uploadedFileElement = findElement(uploadedFile);
        new Actions(getWrappedDriver()).contextClick(uploadedFileElement)
                .click(findElement(getLinkButtonLocator)).build().perform();
    }

    public void switchToNewWindow() {
        for (String winHandle : getWrappedDriver().getWindowHandles()) {
            getWrappedDriver().switchTo().window(winHandle);
        }
    }

    public void clickAllElements(By locator) {
        for (WebElement element : findElements(locator)) {
            click(element);
        }
    }

    public void dragAndDropFile(By draggableLocator, By targetFolderLocator) {
        WebElement draggable = findElement(draggableLocator);
        WebElement targetFolder = findElement(targetFolderLocator);
        waitForElementIsVisible(draggable);
        waitForElementIsVisible(targetFolderLocator);
        new Actions(getWrappedDriver()).dragAndDrop(draggable, targetFolder).perform();//Actions - By locator problem
    }

    public boolean checkItemInRoot(String newFolderName, String pathToWaitElement, By locator) {
        waitForElementIsVisible(
                By.xpath(pathToWaitElement));
        List<WebElement> elements = findElements(locator);
        for (WebElement element : elements) {
            if (getText(element).contains(newFolderName)) {
                return true;
            }
        }
        return false;
    }

    public String getTextFromAlert() {
        Alert alert = getWrappedDriver().switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        return alertText;
    }

    public String getText(By locator) {
        return findElement(locator).getText();
    }

    private String getText(WebElement element) {
        return element.getText();
    }

    public void sendKeys(By locator, String s) {
        findElement(locator).sendKeys(s);
    }

    public void clear(By locator) {
        findElement(locator).clear();
    }

    public void switchToFrame(int numberOfFrame) {
        driver.switchTo().frame(numberOfFrame);
    }

    public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    public boolean isDisplayed(By locator) {
        return findElement(locator).isDisplayed();
    }

    public void waitForElementIsVisible(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, WebDriverFactory.TEN);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void customWait(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, WebDriverFactory.TEN);
        wait.until(ExpectedConditions.urlContains("mail.ru"));
        wait.until(ExpectedConditions.titleContains("Mail.Ru"));
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForElementPresent(By locator) {
        (new WebDriverWait(driver, 15)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return isElementPresent(locator);
            }
        });
    }

    public void waitForElementPresent(WebElement element) {
        (new WebDriverWait(driver, 15)).until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return isElementPresent(element);
            }
        });
    }

    private Boolean isElementPresent(By locator) {
        List elements = driver.findElements(locator);
        return elements.size() > 0;
    }

    private Boolean isElementPresent(WebElement element) {
        return isElementPresent(element);
    }

    public void waitForElementIsVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, WebDriverFactory.TEN);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public boolean checkLetterHasSubject(By locator, String subject) {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //customWait(locator);
       // waitForElementIsVisible(locator);

        List<WebElement> letters = findElements(locator);
        for (WebElement letter : letters) {
            waitForElementIsVisible(letter);
            if (getText(letter).equals(subject)) {
                return true;
            }
        }
        return false;
    }
}


