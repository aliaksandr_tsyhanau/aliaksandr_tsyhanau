package com.epam.tat.framework.ui;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    public static final int TEN = 10;
    public static final int SIXTY = 60;

    public static WebDriver getWebDriver() {

        WebDriver driver;
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(SIXTY, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(SIXTY, TimeUnit.SECONDS);

        //        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        //        eventFiringWebDriver.register(new WebDriverListener());
        //        return EventFiringDriver;
        return driver;
    }
}
