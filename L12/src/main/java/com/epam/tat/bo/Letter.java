package com.epam.tat.bo;

public class Letter {

    private String emailAddress;
    private String subject;
    private String bodyText;

    public String getEmail() {
        return emailAddress;
    }

    public String getSubject() {
        return subject;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }
}
