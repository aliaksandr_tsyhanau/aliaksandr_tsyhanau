package com.epam.tat.bo;

import com.epam.tat.framework.utils.RandomUtils;

public class ItemInCloudFactory {
    public static final String UPLOAD_FILE_NAME = "TestDownloadFile";
    public static final String PATH_TO_UPLOAD_FILE = "\\src\\main\\resources\\TestDownloadFile.txt";

    public static ItemInCloud getFileInCloud() {
        ItemInCloud itemInCloud = new ItemInCloud();
        itemInCloud.setName(UPLOAD_FILE_NAME);
        itemInCloud.setPathToItem(PATH_TO_UPLOAD_FILE);
        return itemInCloud;
    }

    public static ItemInCloud getFolderInCloud() {
        String folderName = RandomUtils.getRandomString();
        ItemInCloud itemInCloud = new ItemInCloud();
        itemInCloud.setName(folderName);
        return itemInCloud;
    }
}
