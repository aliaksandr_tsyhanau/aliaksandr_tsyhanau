package com.epam.tat.test.mail;

import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.MailService;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.tat.bo.LetterFactory.getCorrectLetter;

public class SendMailTest extends BeforeSendTest {

    @Test(description = "After sending letter to correct address with subject"
            + " and body text we will see this letter in Sent and Inbox folders.")
    public void sendMailTest() {

        Letter letter = LetterFactory.getCorrectLetter();
        //System.out.println("SUBJECT 1 IS " + MailService.getLastLetterSubject());

        MailService.sendNewLetter(letter);
        MailService.clickInboxButton();
        // System.out.println("SUBJECT 1.1 IS " + MailService.getLastLetterSubject());

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(MailService.getLastLetterSubject(letter.getSubject() + letter.getBodyText()),
                "Letter is not in inbox. Subject and body are not correct.");

        MailService.moveToSended();
        softAssert.assertTrue(MailService.getLastSendLetterSubject(letter.getSubject() + letter.getBodyText()),
                "Letter is not in sended box. Subject and body are not correct.");
        System.out.println("SUBJECT 3 IS " + MailService.getLastLetterSubject(letter.getSubject() + letter.getBodyText()));
        softAssert.assertAll();
    }
}
