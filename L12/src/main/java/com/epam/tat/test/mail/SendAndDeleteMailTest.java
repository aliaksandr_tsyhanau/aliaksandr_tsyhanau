package com.epam.tat.test.mail;

import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendAndDeleteMailTest extends BeforeSendTest {

    //    @BeforeTest(description = "Create Letter and MailServiceObjects")
    //    public void createDraftLetter() {
    //        Letter letter = LetterFactory.generateLetter(CORRECT_ADDRESS, DRAFT_SUBJECT, DRAFT_TEXT);
    //    }
    //
    //     ?? Why don't work?

    Letter letter = new LetterFactory().getCorrectLetter();

    @Test(description = "After sending letter to correct "
            + "address, and deleting it we will see this letter in Trash folder.")
    public void sendMailTest() {


        MailService.sendNewLetter(letter);
        MailService.clickInboxButton();

        MailService.deleteLastLetter(letter);
        MailService.moveToTrash();
        Assert.assertTrue(MailService.getLastTrashLetterSubject(letter),
                "Letter is not in draft or subject is incorrect");
    }

    @Test(dependsOnMethods = "sendMailTest",
            description = "Delete letter from Trash folder and check that it is not present in Trash.")
    public void deleteMailTest() {

        //Letter letter = new Letter();
        MailService.deleteLastLetter(letter);
        Assert.assertFalse(MailService.getLastTrashLetterSubject(letter),
                "This letter must be deleted from draft");
        //        Assert.assertEquals(trashPackagePage.getLastLetterSubject(), DRAFT_SUBJECT + DRAFT_TEXT,
        //                "This letter must be deleted from draft");
    }
}
