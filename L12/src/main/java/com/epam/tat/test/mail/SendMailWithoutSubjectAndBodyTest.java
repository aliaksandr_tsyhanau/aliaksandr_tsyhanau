package com.epam.tat.test.mail;

import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.MailService;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SendMailWithoutSubjectAndBodyTest extends BeforeSendTest {
    @Test(description = "After sending letter to correct "
            + "address, but without subject and body text we will see this letter in Sent and Inbox folders.")
    public void sendMailWithoutSubjectAndBodyTest() {

        MailService.sendNewLetterWithoutSubjectAndText(LetterFactory.getCorrectLetterWithoutSubjectAndText());
        MailService.clickInboxButton();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertTrue(MailService.getLastLetterSubject("") || MailService.getLastLetterSubject("<Без темы>"),
                "Subject and body are not as in sended letter.");

        MailService.moveToSended();

        softAssert.assertTrue(MailService.getLastSendLetterSubject("")
                        || MailService.getLastSendLetterSubject("<Без темы>"),
                "Subject and body are not as in sended letter.");
        softAssert.assertAll();
    }
}
