package com.epam.tat.test.mail;

import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.LogInService;
import org.testng.annotations.BeforeClass;
import com.epam.tat.test.*;

import static com.epam.tat.bo.AccountFactory.getExistentAccount;
import static com.epam.tat.bo.LetterFactory.CORRECT_ADDRESS;
import static com.epam.tat.bo.LetterFactory.DRAFT_SUBJECT;
import static com.epam.tat.bo.LetterFactory.DRAFT_TEXT;

public class BeforeSendTest extends BeforeAfterTest {

    @BeforeClass(description = "log in mail box with correct name and password")
    public void logInMailRu() {

        LogInService.logInMailBox(getExistentAccount());

        Letter letter = LetterFactory.generateLetter(CORRECT_ADDRESS, DRAFT_SUBJECT, DRAFT_TEXT);
    }
}
