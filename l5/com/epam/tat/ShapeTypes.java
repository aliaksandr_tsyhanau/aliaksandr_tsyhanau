package com.epam.tat;

/**
 * Created by LENOVO on 04.07.2017.
 */
public enum ShapeTypes {
    TRIANGLE,
    SQUARE,
    CIRCLE,
    RECTANGLE;
}
