package com.epam.tat;

/**
 * Created by LENOVO on 03.07.2017.
 */
public interface Shape {
   int area(String shapeName , int a);
   int perimeter(String shapeName, int a);
}

