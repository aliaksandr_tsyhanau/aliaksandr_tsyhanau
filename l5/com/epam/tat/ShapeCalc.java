package com.epam.tat;

/**
 * Created by LENOVO on 03.07.2017.
 */
public class ShapeCalc implements Shape {
    public  int area(String shapeName, int a) {
        int s;
        switch (shapeName) {
            case "triangle":
                s = (int)(Math.sqrt(3) * Math.pow(a, 2) / 4);
                break;
            case "square":
                s = a * a;
                break;
            case "circle":
                s = (int)(Math.PI * Math.pow(a, 2) / 4);
                break;
            default:
                s = 0;
                break;
        }
        return s;
    }

    public  int area(String shapeName, int a, int b) {
            int s = a * b;
            return s;
    }

    public int perimeter(String shapeName, int a) {
        int p;
        switch (shapeName) {
            case "triangle":
                p = a * 3;
                break;
            case "square":
                p = a * 4;
                break;
            case "circle":
                p = (int)(Math.PI * a);
                break;
            default:
                p = 0;
                break;
        }
        return p;
    }

    public int perimeter(String shapeName, int a, int b) {
            int p = (a + b) * 2;
            return p;
    }

    public boolean verifyShapeName(String shapeName){
        int trueCounter = 0;
        for(ShapeTypes type : ShapeTypes.values()){
            if(ShapeTypes.valueOf(shapeName.toUpperCase()) == type) {
                trueCounter++;
            }
        }
        if(trueCounter > 0){
            return true;
        }
        return false;
    }
}
