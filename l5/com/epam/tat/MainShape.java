package com.epam.tat;

import java.util.Arrays;


import static com.sun.org.apache.xml.internal.dtm.Axis.getNames;

/**
 * Created by LENOVO on 04.07.2017.
 */
public class MainShape extends ShapesParser{
    public static void main(String[] args) {
       for (int i = 0; i < args.length; i++)
        shapesPlant(args[i]);
    }

    ShapeCalc shape1 = new ShapeCalc();

    public static void shapesPlant (String inputString){
        String[] arrayWithShapesStrings  = shapesStringParser(inputString);
        String[] arrayWithResults        = new String[arrayWithShapesStrings.length];
        String[] arrayShapesControl      = Arrays.toString(ShapeTypes.values()).replaceAll("^.|.$", "").split(", ");

        for (int i = 0; i < arrayWithShapesStrings.length; i++){
            arrayWithResults[i]             = shapeMaker(arrayWithShapesStrings[i]);
            String[] arrThisShapeResults    = arrayWithResults[i].split("\\.");
            String shapeNameUpperCase       = arrThisShapeResults[0].substring(0,1).toUpperCase() + arrThisShapeResults[0].substring(1);
            System.out.println(shapeNameUpperCase);
            System.out.println("    Area" + ": " + arrThisShapeResults[1]);
            System.out.println("    Perimeter" + ": " + arrThisShapeResults[2]);
        }
    }

    public static String shapeMaker(String shapeString) {
        ShapeCalc shape1     = new ShapeCalc();
        String[] shapeParam  = shapeString.split(":");
        String shapeName     = shapeParam[0];
        shape1.verifyShapeName(shapeName);
        int a   = Integer.parseInt(shapeParam[1]);
        int b   = -1;
        int shapeArea, shapePerimeter;

        if (shapeParam.length > 2) {
            b = Integer.parseInt(shapeParam[2]);
        }
        if (b == -1) {
            shapeArea       = shape1.area(shapeName, a);
            shapePerimeter  = shape1.perimeter(shapeName, a);
            return (shapeName + " - " + a + "." + shapeArea + "." + shapePerimeter);
        } else {
            shapeArea = shape1.area(shapeName, a, b);
            shapePerimeter = shape1.perimeter(shapeName, a, b);
            return (shapeName + " - " + a + " - " + b + "." + shapeArea + "." + shapePerimeter);
        }
    }
}

