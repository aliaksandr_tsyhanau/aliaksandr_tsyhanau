package jbehavetat.steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class MySteps {

    @Given("Actor as user")
    public void givenActorAsUser() {
        System.out.println("1");
    }

    @When("I input correct login and password in cloud.mail.ru")
    public void whenIInputCorrectLoginAndPasswordInCloudmailru() {
        System.out.println("2");
    }

    @Then("I am logged in mail.ru")
    public void thenIAmLoggedInMailru() {
        System.out.println("3");
    }
    
}
