package com.epam.tat.jbehavetat.steps;

import com.epam.tat.bo.Letter;
import com.epam.tat.service.LogInService;
import com.epam.tat.service.MailService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.net.MalformedURLException;

import static com.epam.tat.bo.AccountFactory.getExistentAccount;
import static com.epam.tat.bo.LetterFactory.getIncorrectLetterWithoutAddress;

public class SendMailWithoutAddressSteps {

    private Letter letter;
    private String errorMessage;

    @Given("I am logged")
    public void givenIAmLogged() throws MalformedURLException {
        LogInService.logInMailBox(getExistentAccount());
    }

    @Given("Given I have incorrect letter without address")
    public void GivenIHaveIncorrectLetterWithoutAddress() throws MalformedURLException {
        letter = getIncorrectLetterWithoutAddress();
    }

    @When("I send letter without address")
    public void whenISendLetterWithoutAddress() throws MalformedURLException, InterruptedException {
        errorMessage = MailService.sendNewLetterWithoutAddress(letter);
    }

    @Then("I see error message")
    public void thenISeeErrorMessage() {
        Assert.assertEquals("Message in alert is not correct.",
                "Не указан адрес получателя", errorMessage);
    }
}
