package com.epam.tat.jbehavetat.steps;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.AccountFactory;
import com.epam.tat.logs.Log;
import com.epam.tat.page.mail.LoginPage;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.net.MalformedURLException;

public class NegativeLoginSteps {

    private Account account;

    @Given("Actor as user")
    public void givenActorAsUser() {
        account = AccountFactory.getInvalidAccount();
        Log.info("Invalid account is created");
    }

    @When("I input incorrect <login> and <password> in mail ru")
    public void whenIInputIncorrectLoginAndPasswordInMailRu(@Named("login") String name, @Named("password")
            String password) throws MalformedURLException {
        Log.info("Log in MAIL.RU");
        new LoginPage()
                .openPage()
                .typeLogin(name)
                .typePassword(password)
                .clickSubmitLogIn();
        Log.info("Actor tried to log in with incorrect creditinals");
    }

    @Then("I see message about incorrect login and password")
    public void thenISeeMessageAboutIncorrectLoginAndPassword() throws MalformedURLException {
        Assert.assertEquals("Mailbox can't be opened. Login and password are incorrect.",
                "Неверное имя или пароль", new LoginPage().getIncorrectLoginMessageText());

    }
}
