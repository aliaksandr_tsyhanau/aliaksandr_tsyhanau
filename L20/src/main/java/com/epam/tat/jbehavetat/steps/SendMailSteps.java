package com.epam.tat.jbehavetat.steps;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.LogInService;
import com.epam.tat.service.MailService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.net.MalformedURLException;

import static com.epam.tat.bo.AccountFactory.getExistentAccount;

public class SendMailSteps {

    private Account account;
    private Letter letter;

    @Given("I am logged")
    public void givenIAmLogged() throws MalformedURLException {
        account = getExistentAccount();
        LogInService.logInMailBox(account);
    }

    @Given("I have correct subject and text for letter")
    public void givenIHaveCorrectSubjectAndTextForLetter() {
        letter = LetterFactory.getCorrectLetter();
    }

    @When("I fill all fields of new letter send it from my box to my box")
    public void whenIFillAllFieldsOfNewLetterSendItFromMyBoxToMyBox() throws MalformedURLException {
        MailService.sendNewLetter(letter);
    }

    @Then("I see this letter in sended box")
    public void thenISeeThisLetterInSendedBox() throws MalformedURLException {
        MailService.clickInboxButton();
        MailService.moveToSended();
        Assert.assertTrue("Letter is not in sended box. Subject and body are not correct.",
                MailService.getLastSendLetterSubject(letter.getSubject()));
    }

    @Then("I see this letter in inbox")
    public void thenISeeThisLetterInInbox() throws MalformedURLException {
        MailService.clickInboxBtn();
        Assert.assertTrue("Letter is not in inbox. Subject and body are not correct.",
                MailService.getLastLetterSubject(letter.getSubject()));
    }
}
