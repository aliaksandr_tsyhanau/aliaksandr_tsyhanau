package com.epam.tat.jbehavetat.steps;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.AccountFactory;
import com.epam.tat.logs.Log;
import com.epam.tat.page.mail.InboxPackagePage;
import com.epam.tat.service.LogInService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.net.MalformedURLException;

public class PositiveLoginSteps {

    private Account account;

    @Given("Actor as user")
    public void givenActorAsUser() {
        account = AccountFactory.getExistentAccount();
        Log.info("Account is created");
    }

    @When("I input correct login and password in mail ru")
    public void whenIInputCorrectLoginAndPasswordInMailRu() throws MalformedURLException {
        LogInService.logInMailBox(account);
        Log.info("Actor is logged");
    }

    @Then("I am logged in mail ru")
    public void thenIAmLoggedInMailRu() throws MalformedURLException {
        Assert.assertEquals("Mailbox must be opened. Login and password are correct.",
                account.getName() + "@mail.ru", new InboxPackagePage().getEmailText());
    }
}
