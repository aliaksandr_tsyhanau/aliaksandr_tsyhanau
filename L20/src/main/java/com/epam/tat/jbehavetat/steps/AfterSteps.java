package com.epam.tat.jbehavetat.steps;

import com.epam.tat.service.LogInService;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.ScenarioType;

import java.net.MalformedURLException;

public class AfterSteps {

    @AfterScenario(uponType = ScenarioType.ANY)
    public void quitBrowser() throws MalformedURLException {
        //LogInService.quitBrowser();

    }
}
