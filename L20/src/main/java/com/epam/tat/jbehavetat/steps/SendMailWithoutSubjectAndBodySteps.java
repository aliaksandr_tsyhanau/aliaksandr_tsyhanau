package com.epam.tat.jbehavetat.steps;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.LogInService;
import com.epam.tat.service.MailService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.net.MalformedURLException;

import static com.epam.tat.bo.AccountFactory.getExistentAccount;

public class SendMailWithoutSubjectAndBodySteps {

    private Account account;
    private Letter letter;

    @Given("I am logged")
    public void givenIAmLogged() throws MalformedURLException {
        account = getExistentAccount();
        LogInService.logInMailBox(account);
    }

    @Given("I have correct letter subject and body")
    public void givenIHaveCorrectLetterSubjectAndBody() {
        letter = LetterFactory.getCorrectLetterWithoutSubjectAndText();
    }

    @When("I send this letter")
    public void whenISendThisLetter() throws MalformedURLException {
        MailService.sendNewLetterWithoutSubjectAndText(letter);
    }

    @Then("I see this letter in inbox")
    public void thenISeeThisLetterInInbox() throws MalformedURLException {
        MailService.clickInboxButton();
        Assert.assertTrue("Subject and body are not as in sended letter.",
                MailService.getLastLetterSubject("") || MailService.getLastLetterSubject("<Без темы>"));

    }

    @Then("I see this letter in sended box")
    public void thenISeeThisLetterInSendedBox() throws MalformedURLException {
        MailService.moveToSended();
        Assert.assertTrue("Subject and body are not as in sended letter.",
                MailService.getLastSendLetterSubject("") || MailService.getLastSendLetterSubject("<Без темы>"));
    }
}
