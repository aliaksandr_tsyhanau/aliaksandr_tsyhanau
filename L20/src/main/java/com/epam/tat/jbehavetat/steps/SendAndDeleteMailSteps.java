package com.epam.tat.jbehavetat.steps;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.LogInService;
import com.epam.tat.service.MailService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

import java.net.MalformedURLException;

import static com.epam.tat.bo.AccountFactory.getExistentAccount;

public class SendAndDeleteMailSteps {

    private Account account;
    private Letter letter;

    @Given("I am logged")
    public void givenIAmLogged() throws MalformedURLException {
        account = getExistentAccount();
        LogInService.logInMailBox(account);
    }

    @Given("I have correct letter subject and body")
    public void givenIHaveCorrectLetterSubjectAndBody() {
        letter = LetterFactory.getCorrectLetter();
    }

    @When("I send this letter")
    public void whenISendThisLetter() throws MalformedURLException {
        MailService.sendNewLetter(letter);
    }

    @When("I delete this letter")
    public void whenIDeleteThisLetter() throws MalformedURLException {
        MailService.clickInboxButton();
        MailService.deleteLastLetter(letter);
    }

    @Then("I see this letter in trash box")
    public void thenISeeThisLetterInTrashBox() throws MalformedURLException {
        MailService.moveToTrash();
        Assert.assertTrue("Letter is not in draft or subject is incorrect",
                MailService.getLastTrashLetterSubject(letter));
    }

    @When("I delete this letter from trash")
    public void whenIDeleteThisLetterFromTrash() throws MalformedURLException {
        MailService.deleteLastLetter(letter);
    }

    @Then("I see not this letter in trash box")
    public void thenISeeNotThisLetterInTrashBox() throws MalformedURLException {
        Assert.assertFalse("This letter must be deleted from draft",
                MailService.getLastTrashLetterSubject(letter));
    }
}
