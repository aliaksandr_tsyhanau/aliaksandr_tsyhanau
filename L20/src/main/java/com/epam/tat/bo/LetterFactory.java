package com.epam.tat.bo;

import static com.epam.tat.framework.utils.RandomUtils.getRandomString;

public class LetterFactory {

    public static final String CORRECT_ADDRESS = "lenovot410s@mail.ru";
    public static final String CORRECT_SUBJECT = "lenovo";
    public static final String CORRECT_LETTERSTEXT = "Hello Lenovo!";
    public static final String DRAFT_SUBJECT = "Draft";
    public static final String DRAFT_TEXT = "draft";

    public static Letter getCorrectLetter() {
        Letter letter = new Letter();
        letter.setEmailAddress(CORRECT_ADDRESS);
        letter.setSubject(CORRECT_SUBJECT + getRandomString());
        letter.setBodyText(CORRECT_LETTERSTEXT);
        return letter;
    }

    public static Letter getCorrectLetterWithoutSubjectAndText() {
        Letter letter = new Letter();
        letter.setEmailAddress(CORRECT_ADDRESS);
        letter.setSubject("");
        letter.setBodyText("");
        return letter;
    }

    public static Letter getIncorrectLetterWithoutAddress() {
        Letter letter = new Letter();
        letter.setEmailAddress("");
        letter.setSubject(getRandomString());
        letter.setBodyText(getRandomString());
        return letter;
    }

    public static Letter generateLetter(String address, String subject, String bodyText) {
        Letter letter = new Letter();
        letter.setEmailAddress(address);
        letter.setSubject(subject);
        letter.setBodyText(bodyText);
        return letter;
    }
}
