package com.epam.tat.framework.ui;

import com.epam.tat.logs.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

public final class Browser implements WrapsDriver {

    final int TWENTY = 20;

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();

    private WebDriver driver;

    private Browser() throws MalformedURLException {
        this.driver = WebDriverFactory.getWebDriver();
    }

    public static synchronized Browser getBrowser() throws MalformedURLException {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void close() {
        Log.debug("Before close driver");
        if (getWrappedDriver() != null) {
            getWrappedDriver().quit();
        }
        instance.remove();
    }

    @Override
    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String url) {
        Log.debug("Before open url " + url);
        driver.get(url);
    }

    public WebElement findElement(By locator) {
        Log.debug("Before find element " + locator);
        customWaitForElement(locator);
        return driver.findElement(locator);
    }

    public List<WebElement> findElements(By by) {
        return driver.findElements(by);
    }

    public void click(By locator) {
        Log.debug("Before is visible " + locator);
        customWaitForElement(locator);
        waitForElementIsVisible(locator);

        screenshot();
        Log.debug("Before click " + locator);
        //        ((JavascriptExecutor) driver).executeScript(
        //                "arguments[0].style.border='3px solid red'", findElement(locator));
        customWaitForElement(locator);
        findElement(locator).click();
        //        ((JavascriptExecutor) driver).executeScript(
        //                "arguments[0].style.border='0px'", findElement(locator));
    }

    private void click(WebElement element) {
        Log.debug("Before click " + element);
        waitForElementIsVisible(element);
        screenshot();
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].style.border='3px solid red'", element);
        element.click();
        ((JavascriptExecutor) driver).executeScript(
                "arguments[0].style.border='0px'", element);
    }

    public void rightMouseClick(By uploadedFile, By getLinkButtonLocator) {
        waitForElementIsVisible(uploadedFile);
        WebElement uploadedFileElement = findElement(uploadedFile);
        new Actions(getWrappedDriver()).contextClick(uploadedFileElement)
                .click(findElement(getLinkButtonLocator)).build().perform();
    }

    public void switchToNewWindow() {
        Log.debug("Before switch to new window ");
        for (String winHandle : getWrappedDriver().getWindowHandles()) {
            getWrappedDriver().switchTo().window(winHandle);
        }
    }

    public void clickAllElements(By locator) {
        Log.debug("Before click elements " + locator);
        for (WebElement element : findElements(locator)) {
            click(element);
        }
    }

    public void dragAndDropFile(By draggableLocator, By targetFolderLocator) {
        WebElement draggable = findElement(draggableLocator);
        WebElement targetFolder = findElement(targetFolderLocator);
        waitForElementIsVisible(draggable);
        waitForElementIsVisible(targetFolderLocator);
        new Actions(getWrappedDriver()).dragAndDrop(draggable, targetFolder).perform();
    }

    public boolean checkItemInRoot(String newFolderName, String pathToWaitElement, By locator) {
        waitForElementIsVisible(By.xpath(pathToWaitElement));
        customWaitForElement(By.xpath(pathToWaitElement));
        List<WebElement> elements = findElements(locator);
        for (WebElement element : elements) {
            if (getText(element).contains(newFolderName)) {
                return true;
            }
        }
        return false;
    }

    public String getTextFromAlert() throws InterruptedException {

        //Thread.sleep(5000);

        Alert alert = getWrappedDriver().switchTo().alert();

        String alertText = alert.getText();
        alert.accept();
        return alertText;

    }

    public String getText(By locator) {
        Log.debug("Before get text from " + locator);
        customWaitForElement(locator);
        return findElement(locator).getText();
    }

    public String getText(WebElement element) {
        waitForElementIsVisible(element);
        Log.debug("Before get text from " + element);
        return element.getText();
    }

    public void sendKeys(By locator, String s) {
        Log.debug("Before set text " + s + " to " + findElement(locator));
        customWaitForElement(locator);
        findElement(locator).sendKeys(s);
    }

    public void clear(By locator) {
        Log.debug("Before clear element " + locator);
        customWaitForElement(locator);
        findElement(locator).clear();
    }

    public void switchToFrame(int numberOfFrame) {
        driver.switchTo().frame(numberOfFrame);
    }

    public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    public boolean isDisplayed(By locator) {
        customWaitForElement(locator);
        Log.debug("Before element " + locator + "is displayed.  " + findElements(locator).size());
        return findElements(locator).size() > 0;
    }

    public boolean isVisible(By locator) {
        customWaitForElement(locator);
        return findElement(locator).isDisplayed();
    }

    public void waitForElementIsVisible(By locator) {
        Log.debug("Before element is visible " + locator);
        WebDriverWait wait = new WebDriverWait(driver, WebDriverFactory.SIXTY);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    public void waitForElementIsVisible(WebElement element) {
        Log.debug("Before element is visible " + element);
        WebDriverWait wait = new WebDriverWait(driver, WebDriverFactory.SIXTY);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    public void customWaitForElement(final By locator) {
        new WebDriverWait(driver, TWENTY) {
            public boolean apply(WebDriver driver) {
                return findElements(locator).size() > 0;
            }
        };
    }

    public void screenshot() {
        File screenshotFile = new File(
                "screenshots/log" + System.nanoTime() + ".png");
        try {
            byte[] screenshotsByts = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotsByts);
            Log.debug("Screenshot taken: file://" + screenshotFile.getAbsolutePath());

            Log.debug("<a href=”%link_to_screenshot%” target=”blank”>" + screenshotFile + "</a>");
            Log.debug("<a href=”%link_to_screenshot%” target=”blank”>" + screenshotFile.getAbsolutePath() + "</a>");
            Log.debug("<a> <img src=" + screenshotFile.getAbsolutePath() + "> LINK to hardscreen </a>");

            Reporter.log("<img src=\"/screenshots/log188324543493492.png\"/>");
            Reporter.log("<img src=\"C:\\EPAM\\HometasksTAT2017\\L14\\screenshots\\log189596377922827.png\">");

        } catch (IOException e) {
            throw new RuntimeException("Failed to write screenshot: ", e);
        }
    }
}


