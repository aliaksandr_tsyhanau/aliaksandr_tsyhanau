package com.epam.tat.logs;

import org.apache.log4j.Logger;
//import org.apache.log4j.PropertyConfigurator;

public class Log {

    //TRACE < DEBUG < INFO < WARN < ERROR < FATAL

    //    static {
    //        PropertyConfigurator.configureAndWatch("./src//main//resources//log4j.properties");
    //        //Parameters.instance().get
    //    }

    //private static final Logger log = Logger.getLogger(Log.class);
    private static final Logger log = Logger.getLogger("com.epam.tat");
    //private static final Logger log = Logger.getRootLogger();

    public static void trace(Object message) {
        log.debug(message);
    }

    public static void debug(Object message) {
        log.debug(message);
    }

    public static void info(Object message) {
        log.info(message);
    }

    public static void warn(Object message) {
        log.info(message);
    }

    public static void error(Object message, Throwable t) {
        log.error(message, t);
    }

    public static void error(Object message) {
        log.error(message);
    }

    public static void fatal(Object message) {
        log.info(message);
    }
}
