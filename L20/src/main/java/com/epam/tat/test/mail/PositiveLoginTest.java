package com.epam.tat.test.mail;

import com.epam.tat.bo.Account;
import com.epam.tat.page.mail.InboxPackagePage;
import com.epam.tat.service.LogInService;
import com.epam.tat.test.BeforeAfterTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.net.MalformedURLException;

import static com.epam.tat.bo.AccountFactory.getExistentAccount;


public class PositiveLoginTest extends BeforeAfterTest {

    @Test(description = "Correct input of login and password opens mail box.")
    public void positiveLoginTest() throws MalformedURLException, InterruptedException {

        Account account = getExistentAccount();
        LogInService.logInMailBox(account);
//Thread.sleep(5000);
        Assert.assertEquals(new InboxPackagePage().getEmailText(), account.getName() + "@mail.ru",
                "Mailbox must be opened. Login and password are correct.");
    }
}
