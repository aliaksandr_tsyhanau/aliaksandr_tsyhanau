package com.epam.tat.test.mail;

import com.epam.tat.bo.Letter;
import com.epam.tat.bo.LetterFactory;
import com.epam.tat.service.MailService;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.net.MalformedURLException;

public class SendMailTest extends BeforeSendTest {

    @Test(description = "After sending letter to correct address with subject"
            + " and body text we will see this letter in Sent and Inbox folders.")
    public void sendMailTest() throws MalformedURLException {

        Letter letter = LetterFactory.getCorrectLetter();
        MailService.sendNewLetter(letter);

        SoftAssert softAssert = new SoftAssert();

        MailService.clickInboxButton();
        MailService.moveToSended();
        softAssert.assertTrue(MailService.getLastSendLetterSubject(letter.getSubject()),
                "Letter is not in sended box. Subject and body are not correct.");

        MailService.clickInboxBtn();
        softAssert.assertTrue(MailService.getLastLetterSubject(letter.getSubject()),
                "Letter is not in inbox. Subject and body are not correct.");

        softAssert.assertAll();
    }
}
