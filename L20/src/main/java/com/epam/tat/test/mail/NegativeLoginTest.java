package com.epam.tat.test.mail;

import com.epam.tat.bo.AccountFactory;
import com.epam.tat.page.mail.LoginPage;
import com.epam.tat.service.LogInService;
import com.epam.tat.test.BeforeAfterTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class NegativeLoginTest extends BeforeAfterTest {

    @Test(description = "Incorrect input of login and password opens warning message.")
    public void negativeLoginTest() throws MalformedURLException {

        LogInService.logInMailBox(AccountFactory.getInvalidAccount());

        Assert.assertEquals(new LoginPage().getIncorrectLoginMessageText(), "Неверное имя или пароль",
                "Mailbox can't be opened. Login and password are incorrect.");
    }
}

