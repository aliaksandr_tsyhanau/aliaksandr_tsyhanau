package com.epam.tat.page.mail;

import com.epam.tat.logs.Log;
import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.NotFoundException;
import java.net.MalformedURLException;

public class PageSwitcher extends AbstractPage {

    private static final By INBOX_PACKAGE = By.xpath(
            "//a[@href='/messages/inbox/']//span[contains(@class, 'b-nav__item__text')]");
    private static final By COMPOSE_BUTTON = By.xpath("(//a[@data-name='compose'])[1]");
    private static final By SEND_PACKAGE = By.xpath("//a[@href='/messages/sent/']");
    private static final By TRASH_PACKAGE = By.xpath("(//span[@class='b-nav__item__text'])[4]");
    private static final By KASPERSY_ITEM = By.xpath("//*[@class='icon icon_kav-protected']");

    public PageSwitcher() throws MalformedURLException {
        super();
    }

    public void clickSendButton() {
        browser.waitForElementIsVisible(SEND_PACKAGE);
        browser.click(SEND_PACKAGE);
    }

    public void clickTrashButton() {
        browser.waitForElementIsVisible(TRASH_PACKAGE);
        browser.click(TRASH_PACKAGE);
    }

    public void clickComposeButton() {
        browser.waitForElementIsVisible(COMPOSE_BUTTON);
        browser.click(COMPOSE_BUTTON);
    }

    public void clickInboxBtn() {
        browser.click(INBOX_PACKAGE);
    }

    public boolean checkThatLetterHasSubject(String subject) {
        browser.customWaitForElement(By.xpath(String.format("//*[contains(text(), '%s')]", subject)));
        browser.customWaitForElement(KASPERSY_ITEM);
        By lastLetter = By.xpath(String.format("//*[contains(text(), '%s')]", subject));
        try {
            if (browser.isVisible(lastLetter)) {
                return true;
            }
        } catch (NotFoundException e) {
            Log.error(lastLetter + "! is not displayd !" + e);
        }
        return false;
    }
}
