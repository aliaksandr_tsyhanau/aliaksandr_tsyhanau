package com.epam.tat.page.mail;

import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class SendPackagePage extends PageSwitcher {

    private static final By LAST_LETTER = By.xpath(
            "//div[contains(@data-cache-key, 'undefined_false') and not(contains(@style, 'display: none'))]"
                    + "//div[@class='b-datalist__item__subj']");
    //("//div[contains(text(), 'lenovot410s@mail.ru')] [@class='b-datalist__item__addr']/preceding-sibling::div ");
    //("//div[@class='b-datalist__item__subj']");
    //   //div[not(ancestor::div[@style='display: none'])] [@class='b-datalist__item__subj']

    public SendPackagePage() throws MalformedURLException {
        super();
    }

    public boolean checkLetterHasSubject(String subject) {
        return checkThatLetterHasSubject(subject);

    }
}
