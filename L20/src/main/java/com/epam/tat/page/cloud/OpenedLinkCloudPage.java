package com.epam.tat.page.cloud;

import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class OpenedLinkCloudPage extends AbstractPage {

    private static final By UPLOADED_FILE = By.xpath("//a[@data-name='link-action']");

    public OpenedLinkCloudPage() throws MalformedURLException {
        super();
    }

    public String getNameOfLinkOpensFile() {
        return browser.getText(UPLOADED_FILE);
    }
}
