package com.epam.tat.page.cloud;

import com.epam.tat.page.AbstractPage;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class LoginCloudPage extends AbstractPage {

    private static final String URL_MAIL_RU_CLOUD = "https://cloud.mail.ru";
    private static final By BUTTON_TO_LOGIN = By.xpath("//input[@class='nav-inner-col-try__bt' and  @type='button']");
    private static final By LOGIN_INPUT = By.xpath("//*[@id='ph_login']");
    private static final By PASSWORD_INPUT = By.xpath("//*[@id='ph_password']");
    //private static final By SUBMIT_BUTTON = By.xpath("//input[@class='x-ph__button__input']");
    private static final By SUBMIT_BUTTON = By.xpath("//span[@data-action='login']");

    public LoginCloudPage() throws MalformedURLException {
        super();
    }

    public LoginCloudPage openPage() {
        browser.open(URL_MAIL_RU_CLOUD);
        browser.click(BUTTON_TO_LOGIN);
        return this;
    }

    public LoginCloudPage typeLogin(String login) {
        browser.waitForElementIsVisible(LOGIN_INPUT);
        browser.clear(LOGIN_INPUT);
        browser.sendKeys(LOGIN_INPUT, login);
        return this;
    }

    public LoginCloudPage typePassword(String password) {
        browser.waitForElementIsVisible(PASSWORD_INPUT);
        browser.clear(PASSWORD_INPUT);
        browser.sendKeys(PASSWORD_INPUT, password);
        return this;
    }

    public MainCloudPage clickSubmitButton() throws MalformedURLException {
        browser.click(SUBMIT_BUTTON);
        return new MainCloudPage();
    }
}
