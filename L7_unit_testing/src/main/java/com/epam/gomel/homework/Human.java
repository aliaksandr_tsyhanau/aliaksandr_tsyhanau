package com.epam.gomel.homework;

public interface Human {

     Mood getMood();
}