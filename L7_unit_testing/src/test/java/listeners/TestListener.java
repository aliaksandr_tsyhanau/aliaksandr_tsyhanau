package listeners;

import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

/**
 * Created by LENOVO on 18.07.2017.
 */
public class TestListener implements IInvokedMethodListener {
    @Override
    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(String.format(String.format(String.format("[METHOD_STARTED] - " + iInvokedMethod.getDate()))));
    }

    @Override
    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        System.out.println(String.format("[METHOD_FINISHED] - " + iInvokedMethod.getTestResult()));
    }
}
