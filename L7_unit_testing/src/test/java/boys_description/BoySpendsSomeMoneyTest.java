package boys_description;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.junit.rules.ExpectedException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.Month.*;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class BoySpendsSomeMoneyTest extends BoyBeforeTest {

    @DataProvider(name = "datesForSpendMoney")
    public Object[][] parametersForSpendMoney() {
        return new Object[][]{
                {10, 5.2, 4.8},
                {1000001, 999999, 2},
                {0, -0.1, 0.1}

        };
    }

    @Test(description = "SpendSomeMoney() must return sum of wealth and amountForSpending",
            dataProvider = "datesForSpendMoney", priority = 2)
    public void SpendsSomeMoneyTest(double wealth, double amountForSpending, double newWealth) {
        boy.setWealth(wealth);
        boy.spendSomeMoney(amountForSpending);
        Assert.assertEquals(boy.getWealth(), newWealth);
    }

    @Test(description = "SpendSomeMoney() must return RuntimeException", expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = "Not enough money! Requested amount is.*", priority = 1)
    public void SpendsSomeMoneyExeptionTest() {

        boy.setWealth(999);
        boy.spendSomeMoney(999.01);

    }
}

