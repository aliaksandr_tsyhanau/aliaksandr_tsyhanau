package boys_description;

import com.epam.gomel.homework.Boy;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.Month.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.equalTo;


/**
 * Created by LENOVO on 17.07.2017.
 */
public class BoyIsRichTest extends BoyBeforeTest {

    @DataProvider (name = "dates for BoyIsRich Test")
    public Object[][] datesForBoyIsRichTest(){
        return new Object[][]{
                {2200000, true},
                {1000000.01, true},
                {999999, false},
               {0, false},
                {-23, false}
        };
    }

    @Test (description = "For wealth more as 1 000 000 isRich must be TRUE", dataProvider = "dates for BoyIsRich Test")
    public void boyIsRich(double wealth, boolean b){
        boy.setWealth(wealth);
        //Assert.assertEquals(boy.isRich(), b);
        assertThat("Not correct isRich()", boy.isRich(), both(notNullValue()).and(equalTo(b)));
    }
}
