package boys_description;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.JUNE;
import static com.epam.gomel.homework.Month.MAY;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class BoyHasPrettyGirlFriendTest extends BoyBeforeTest {

    @Test (description = "for isPretty=true isPrettyGirlFriend() must be TRUE")
    public void hasPrettyGirlFriend(){

        SoftAssert softAssert = new SoftAssert();

        boy.setGirlFriend(new Girl(true));
        softAssert.assertEquals(boy.isPrettyGirlFriend(), true);

        boy.setGirlFriend(new Girl(false));
        softAssert.assertEquals(boy.isPrettyGirlFriend(), false);

        boy.setGirlFriend(new Girl());
        softAssert.assertEquals(boy.isPrettyGirlFriend(), false);

        softAssert.assertAll();
    }

}