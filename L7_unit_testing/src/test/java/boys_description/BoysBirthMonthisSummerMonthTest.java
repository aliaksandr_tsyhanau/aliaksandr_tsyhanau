package boys_description;

import com.epam.gomel.homework.Boy;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.*;
import static com.sun.javaws.JnlpxArgs.verify;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class BoysBirthMonthisSummerMonthTest  extends BoyBeforeTest {

    @Test (description = "for summermonths isSummerMonths() must be TRUE")
    public void isSummerMonthTest(){
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(boy.isSummerMonth(), false);

        boy.setBirthdayMonth(JUNE);
        softAssert.assertEquals(boy.isSummerMonth(), true);

        boy.setBirthdayMonth(JULY);
        softAssert.assertEquals(boy.isSummerMonth(), true);

        boy.setBirthdayMonth(AUGUST);
        softAssert.assertEquals(boy.isSummerMonth(), true);

        boy.setBirthdayMonth(SEPTEMBER);
        softAssert.assertEquals(boy.isSummerMonth(), false);

        softAssert.assertAll();


//MOCKITO
        Boy mockedBoy = Mockito.mock(Boy.class);
        mockedBoy.setBirthdayMonth(MAY);
        Assert.assertEquals(mockedBoy.isSummerMonth(), false);
        //verify(mockedBoy).setBirthdayMonth(SEPTEMBER);
        //when(mockedBoy.setBirthdayMonth(SEPTEMBER).return("Birthday is September");



    }


}
