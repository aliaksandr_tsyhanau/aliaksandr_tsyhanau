package boys_description;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Month;
import org.testng.annotations.BeforeClass;

import static com.epam.gomel.homework.Month.MARCH;
import static com.epam.gomel.homework.Month.MAY;

/**
 * Created by LENOVO on 18.07.2017.
 */
public class BoyBeforeTest {

    protected Boy boy;

    @BeforeClass
    public void setUp() {
        boy = new Boy(MARCH);
    }
}
