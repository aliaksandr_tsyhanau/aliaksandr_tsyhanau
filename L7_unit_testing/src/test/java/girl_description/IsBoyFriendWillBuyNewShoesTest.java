package girl_description;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.JULY;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class IsBoyFriendWillBuyNewShoesTest extends GirlBeforeTest {



    @Test(description = "If boyfriend is reach and girl is pretty isBoyFriendWillBuyNewShoes() must return TRUE.")
    public void boyFriendWillBuyNewShoes() {
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertFalse(girl.isBoyFriendWillBuyNewShoes(), "Mistake. Girl is not pretty. Boy exists not.");

        girl.setBoyFriend(new Boy(JULY, 1_000_001));
        softAssert.assertFalse(girl.isBoyFriendWillBuyNewShoes(), "Mistake. Girl is not pretty.");

        girl.setPretty(true);
        girl.getBoyFriend().setWealth(1_500_500);
        softAssert.assertTrue(girl.isBoyFriendWillBuyNewShoes(), "Mistake. Girl is  pretty. Boy is rich.");

        softAssert.assertAll();
    }

}
