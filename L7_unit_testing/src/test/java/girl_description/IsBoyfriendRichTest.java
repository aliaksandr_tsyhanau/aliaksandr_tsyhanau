package girl_description;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Human;
import com.epam.gomel.homework.Month;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.internal.MockitoCore;
import org.mockito.junit.MockitoJUnitRunner;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.mockito.Mock;
import org.testng.asserts.SoftAssert;

import static org.mockito.Mockito.*;
import java.util.Iterator;

import static com.epam.gomel.homework.Month.JULY;
import static com.epam.gomel.homework.Month.SEPTEMBER;

/**
 * Created by LENOVO on 17.07.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class IsBoyfriendRichTest extends GirlBeforeTest {
//private  Human human;
//private  Girl girl;
//
//        @Before
//    public void setUpMockGirl(){
//        human = Mockito.mock(Human.class);
//        girl = new Girl();
//
//    }

    @Test(description = "If boyfriend exists ans he is reach isBoyfriendRich() must return TRUE.")
    public void isBoyfriendRich(){
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertFalse(girl.isBoyfriendRich(), "Boy cannot be rich. He exists not.");

        girl.setBoyFriend(new Boy(JULY, 2_000_000));
        softAssert.assertTrue(girl.isBoyfriendRich(), "Boys wealth is 2_000_000. How can he be not rich?");

        girl.setBoyFriend(new Boy(SEPTEMBER, 999_999.99));
        softAssert.assertFalse(girl.isBoyfriendRich(), "Boy cannot be rich. His wealth is less as 1_000_000.");

        softAssert.assertAll();

    }
}
