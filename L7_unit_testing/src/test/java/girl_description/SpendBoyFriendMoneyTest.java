package girl_description;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Month;
import listeners.TestListener;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;


/**
 * Created by LENOVO on 17.07.2017.
 */
@Listeners({TestListener.class})
public class SpendBoyFriendMoneyTest extends GirlBeforeTest {

    @Test(description = "For wealth of friendBoy more as 1 000 000 spendBoyFriendMoney() must make wealth less for amountForSpending")
    public void spendBoyFriendMoney() {
        girl.setBoyFriend(new Boy(Month.JULY));
        girl.getBoyFriend().setWealth(1_111_111);
        girl.spendBoyFriendMoney(111_111.0);
        Assert.assertEquals(girl.getBoyFriend().getWealth(), 1_000_000.0, "Wealth must be 1 000 000.");
    }

    @Test(description = "For wealth of friendBoy more as 1 000 000 spendBoyFriendMoney() must make wealth less for amountForSpending",
            expectedExceptionsMessageRegExp = "Not enough money! Requested amount is.*", priority = 1)
    public void spendBoyFriendMoneyException() {
        girl.setBoyFriend(new Boy(Month.JULY));
        girl.getBoyFriend().setWealth(0);
        girl.spendBoyFriendMoney(111.0);
    }
}

