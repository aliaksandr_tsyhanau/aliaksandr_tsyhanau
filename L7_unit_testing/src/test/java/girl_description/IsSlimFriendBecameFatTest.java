package girl_description;

import org.apache.tools.ant.taskdefs.condition.IsTrue;
import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsInstanceOf.instanceOf;

/* Created by LENOVO on 17.07.2017.
*/
public class IsSlimFriendBecameFatTest extends GirlBeforeTest {

    @Test(description = "If girl is not pretty  and isSlimFriendGotAFewKilos()=true isSlimFriendBecameFat() must return TRUE.",
            priority = 0)
    @Parameters({"SlimFriendGotAFewKilos", "isPretty"})
    public void slimFriendBecameFat1(boolean SlimFriendGotAFewKilos, boolean isPretty) {
        girl.setSlimFriendGotAFewKilos(SlimFriendGotAFewKilos);
        girl.setPretty(isPretty);
        //Assert.assertTrue(girl.isSlimFriendBecameFat(), "It must return true.");
        assertThat("It must return true.", girl.isSlimFriendBecameFat(), both(instanceOf(Boolean.class)).
                and(equalTo(true)));
    }

    @Test(description = "If girl is pretty  and isSlimFriendGotAFewKilos()=true isSlimFriendBecameFat() must return FALSE.",
            dependsOnMethods = "slimFriendBecameFat1", alwaysRun = true, priority = 1)
    @Parameters({"SlimFriendGotAFewKilos"})
    public void slimFriendBecameFat2(boolean SlimFriendGotAFewKilos) {
        girl.setSlimFriendGotAFewKilos(SlimFriendGotAFewKilos);
        girl.setPretty(true);
        //Assert.assertFalse(girl.isSlimFriendBecameFat(), "It must return false.");
        assertThat("It must return true.", girl.isSlimFriendBecameFat(),both(instanceOf(Boolean.class)).
                and(equalTo(false)).and(notNullValue()));
    }

    @Test(description = "If girl is pretty  and isSlimFriendGotAFewKilos()=false isSlimFriendBecameFat() must return FALSE.",
            dependsOnMethods = "slimFriendBecameFat2", alwaysRun = true, priority = 2)
    public void slimFriendBecameFat3() {
        girl.setSlimFriendGotAFewKilos(false);
        girl.setPretty(true);
       // Assert.assertFalse(girl.isSlimFriendBecameFat(), "It must return false.");
        assertThat("It must return true.", girl.isSlimFriendBecameFat(), both(instanceOf(Boolean.class)).
                and(equalTo(false)));
    }
}
