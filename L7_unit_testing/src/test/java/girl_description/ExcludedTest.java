package girl_description;

import org.testng.annotations.Test;

/**
 * Created by LENOVO on 18.07.2017.
 */
public class ExcludedTest {
    @Test(description = "I will not run this test", groups = "excluded", priority = 3)
    public void excludedTest() {
        System.out.println("Yor can't see me. I was excluded.");
    }
}
