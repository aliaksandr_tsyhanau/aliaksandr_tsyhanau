package boys_constructors;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;
import java.util.regex.Matcher;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.*;

import static com.epam.gomel.homework.Month.*;
import static sun.nio.cs.Surrogate.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Every.everyItem;


/**
 * Created by LENOVO on 15.07.2017.
 */
public class CreateBoyWithThreeParametersTest {

    private Month month;
    private double wealth;
    private Girl girl;

    @Factory (dataProvider = "datesForBoyConstructors")
    public CreateBoyWithThreeParametersTest(Month month, double wealth, Girl girl) {
        this.month = month;
        this.wealth = wealth;
        this.girl = girl;
    }

    @DataProvider (name = "datesForBoyConstructors")
    public static Object[][] parametersForBoyConstuctors(){
        return new Object[][]{
                {JANUARY, 2.32, new Girl()},
                {MARCH, 0.0, new Girl(true, true, null)},
                {DECEMBER, -5.0, new Girl(false, false)},
                {AUGUST, 1000555.0, new Girl(true)}
        };
    }

    @Test (description = "Boy constructors with three parameters create Boy objects")
    public void CreateBoyWithThreeParameters() {
        Boy boy = new Boy(month, wealth, girl);
       assertThat("Not correct birthdayMonth", boy.getBirthdayMonth(), both(notNullValue()).and(equalTo(month)));
        //assertThat(list, both(hasSize(1)).and(contains(42)));

        //Assert.s
       //assertThat(" ".isA(String.class));
        //assertThat("  ", isNotNull());
        //assertThat("123", equals("123"));
       // assertThat("Hello World", is(allOf(notNullValue(),isA(String))));
        //assertThat("Hello World", is(allOf(notNullValue(),instanceOf(String.class))));
//        Assert.assertEquals(boy.getBirthdayMonth(), month, "Not correct birthdayMonth");
//        Assert.assertEquals(boy.getWealth(), wealth, "Not correct wealth value");
//        Assert.assertEquals(boy.getGirlFriend(), girl, "Not correct girlfriend");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(boy.getBirthdayMonth(), month, "Not correct birthdayMonth");
        softAssert.assertEquals(boy.getWealth(), wealth, "Not correct wealth value");
        softAssert.assertEquals(boy.getGirlFriend(), girl, "Not correct girlfriend");
        softAssert.assertAll();
    }

//    @Test (description = "Boy constructors with three parameisters create Boy objects")
//    public static Matcher test() {
//    }


}
