package boys_constructors;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.*;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class CreateBoyWithOneParameterTest {

    @DataProvider(name = "datesForBoyConstructors")
    public Object[][] parametersForBoyConstuctors(){
        return new Object[][]{
                {OCTOBER},
                {MAY},
                {JULY},
        };
    }

    @Test(description = "Boy constractors with one parameter create Boy objects",
            dataProvider = "datesForBoyConstructors")
    public void test(Month month){
        Boy boy = new Boy(month);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(boy.getBirthdayMonth(), month, "Not correct birthdayMonth");
        softAssert.assertEquals(boy.getWealth(), 0.0, "Not correct wealth value");
        softAssert.assertNull(boy.getGirlFriend(), "Not correct girlfriend");
        softAssert.assertAll();
    }
}
