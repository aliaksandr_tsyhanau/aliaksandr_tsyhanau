package boys_constructors;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.*;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class CreateBoyWithTwoParametersTest {

    private Month month;
    private double wealth;

    @Factory(dataProvider = "datesForBoyConstructors")
    public CreateBoyWithTwoParametersTest(Month month, double wealth) {
        this.month = month;
        this.wealth = wealth;
    }

    @DataProvider(name = "datesForBoyConstructors")
    public static Object[][] parametersForBoyConstuctors() {
        return new Object[][]{
                {FEBRUARY, -2.32},
                {MAY, 99.99},
                {JULY, -1000001},
        };
    }

    @Test(description = "Boy constractors with two parameters create Boy objects")
    public void createBoyWithTwoParameters() {
        Boy boy = new Boy(month, wealth);

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(boy.getBirthdayMonth(), month, "Not correct birthdayMonth");
        softAssert.assertEquals(boy.getWealth(), wealth, "Not correct wealth value");
        softAssert.assertNull(boy.getGirlFriend(), "Not correct girlfriend");
        softAssert.assertAll();
    }

}
