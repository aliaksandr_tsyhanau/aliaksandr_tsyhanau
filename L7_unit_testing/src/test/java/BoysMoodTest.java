import boys_description.BoyBeforeTest;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.Month.*;
import static com.epam.gomel.homework.Mood.*;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class BoysMoodTest extends BoyBeforeTest {

    @DataProvider(name = "dates for Boys mood test")
    public Object[][] parametersForBoysMoodTest() {
        return new Object[][]{
                {5000000.0, true, JUNE, EXCELLENT},
                {5000000.0, true, MAY, GOOD},
                {5000000.0, false, SEPTEMBER, BAD},
                {11.0, false, DECEMBER, HORRIBLE},
                {11.0, false, JUNE, BAD},
                {11.0, true, AUGUST, BAD}
        };
    }

    @Test(description = "Boys mood must depence from combination of three parameters",
            dataProvider = "dates for Boys mood test")
    public void boyMoodTest(double wealth, boolean prettyGirl, Month month, Mood mood) {
        boy.setWealth(wealth);
        boy.setGirlFriend(new Girl());
        boy.getGirlFriend().setPretty(prettyGirl);
        //System.out.println(boy.getGirlFriend().isPretty());
       boy.setBirthdayMonth(month);
       Assert.assertEquals(boy.getMood(), mood);
    }
}
