package girls_constructors;

import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static javax.swing.SwingWorker.StateValue.STARTED;
import static jdk.internal.org.objectweb.asm.Type.METHOD;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class CreateGirlWithOneParamrterTest {

    @DataProvider(name = "datesForGirlConstructors")
    public Object[][] parametersForGirlConstuctors(){
        return new Object[][]{
                {true},
                {false}
        };
    }

    @Test(description = "Girl constructors with one parameter create Girl objects",
            dataProvider = "datesForGirlConstructors")
    public void test(boolean isPretty){
        Girl girl = new Girl(isPretty);
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(girl.isPretty(), isPretty, "Not correct pretty boolean");
        softAssert.assertEquals(girl.isSlimFriendGotAFewKilos(), false, "Not correct boolean isSlimFriendGotAFewKilos");
        softAssert.assertNull(girl.getBoyFriend(), "Not correct boyfriend");

        softAssert.assertAll();

    }


}

