package girls_constructors;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.*;

/**
 * Created by LENOVO on 16.07.2017.
 */
public class CreateGirlWithThreeParamrtersTest {
    @DataProvider(name = "datesForGirlConstructors")
    public Object[][] parametersForGirlConstuctors(){
        return new Object[][]{
                {true, true, new Boy(APRIL, 0, new Girl())},
                {true, false, new Boy(MAY, 0.01, new Girl(true))},
                {false, true, new Boy(NOVEMBER)},
                {false, false, new Boy(JUNE)}
        };
    }

    @Test(description = "Girl constructors with three parameters create Girl objects",
            dataProvider = "datesForGirlConstructors")
    public void test(boolean isPretty, boolean isSlimFriendGotAFewKilos, Boy boy){
        Girl girl = new Girl(isPretty, isSlimFriendGotAFewKilos, boy);
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(girl.isPretty(), isPretty, "Not correct pretty boolean");
        softAssert.assertEquals(girl.isSlimFriendGotAFewKilos(), isSlimFriendGotAFewKilos, "Not correct boolean isSlimFriendGotAFewKilos");
        softAssert.assertEquals(girl.getBoyFriend(), boy, "Not correct boyfriend");

        softAssert.assertAll();
    }
}

