package girls_constructors;

import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.epam.gomel.homework.Month.*;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class CreateGirlWithTwoParamrtersTest {

    @DataProvider(name = "datesForGirlConstructors")
    public Object[][] parametersForGirlConstuctors(){
        return new Object[][]{
                {true, true},
                {true, false},
                {false, true},
                {false, false}
        };
    }

    @Test(description = "Girl constructors with two parameters create Girl objects",
            dataProvider = "datesForGirlConstructors")
    public void test(boolean isPretty, boolean isSlimFriendGotAFewKilos){
        Girl girl = new Girl(isPretty, isSlimFriendGotAFewKilos);
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(girl.isPretty(), isPretty, "Not correct pretty boolean");
        softAssert.assertEquals(girl.isSlimFriendGotAFewKilos(), isSlimFriendGotAFewKilos, "Not correct boolean isSlimFriendGotAFewKilos");
        softAssert.assertNull(girl.getBoyFriend(), "Not correct boyfriend");

        softAssert.assertAll();
    }
}
