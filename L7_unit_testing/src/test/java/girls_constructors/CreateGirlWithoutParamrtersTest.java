package girls_constructors;

import com.epam.gomel.homework.Girl;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

/**
 * Created by LENOVO on 17.07.2017.
 */
public class CreateGirlWithoutParamrtersTest {

    @Test(description = "Girl constructor without parameters create Girl objects")
    public void test() {
        Girl girl = new Girl();
        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(girl.isPretty(), false, "Not correct pretty boolean");
        softAssert.assertEquals(girl.isSlimFriendGotAFewKilos(), true, "Not correct boolean isSlimFriendGotAFewKilos");
        softAssert.assertNull(girl.getBoyFriend(), "Not correct boyfriend");

        softAssert.assertAll();
    }
}
