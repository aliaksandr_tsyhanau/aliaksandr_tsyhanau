package runner;

import junit.framework.TestListener;
import org.testng.TestNG;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

/**
 * Created by LENOVO on 18.07.2017.
 */
public class TestRunner {
    public static void main(String[] args){
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList("./src//test//resources//testng.xml");
        testNG.setTestSuites(files);
        //testNG.addListener(TestListener.class);
        testNG.run();
    }
}
