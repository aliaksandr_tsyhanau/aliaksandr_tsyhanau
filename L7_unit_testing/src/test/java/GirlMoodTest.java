import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.Month.JULY;
import static com.epam.gomel.homework.Mood.*;

/**
 * Created by LENOVO on 18.07.2017.
 */
public class GirlMoodTest {

    private Girl girl;

    @BeforeClass
    public void setUp() {
        girl = new Girl(true, true, new Boy(JULY, 5_000_000));
    }

    @Test(description = "Girl's mood must be Excellent, if girl is pretty and boy is rich.", priority = 0)
    public void girlMoodExcellentTest() {
        Assert.assertEquals(girl.getMood(), EXCELLENT, "Mood must be excellent.");
    }

    @Test(description = "Girl's mood must be Good, if girl is pretty and boy is not rich.", priority = 1)
    public void girlMoodGoodTest1() {
        girl.getBoyFriend().setWealth(0);
        Assert.assertEquals(girl.getMood(), GOOD, "Mood must be good.");
    }

    @Test(description = "Girl's mood must be Good, if girl is not pretty and boy is rich.", priority = 2)
    public void girlMoodGoodTest2() {
        girl.getBoyFriend().setWealth(4_000_000);
        girl.setPretty(false);
        Assert.assertEquals(girl.getMood(), GOOD, "Mood must be good.");
    }

    @Test(description = "Girl's mood must be Neutral, if girl is not pretty and boy is not rich," +
            "but isSlimFriendBecameFat() = true.", priority = 3)
    public void girlMoodNeutralTest() {
        girl.getBoyFriend().setWealth(100.05);
        Assert.assertEquals(girl.getMood(), NEUTRAL, "Mood must be neutral.");
    }

    @Test(description = "Girl's mood must be Neutral, if girl is not pretty, boy is not rich" +
            "and isSlimFriendBecameFat() = false.", priority = 4)
    public void girlI_HATE_THEM_ALLTest() {
        girl.setSlimFriendGotAFewKilos(false);
        Assert.assertEquals(girl.getMood(), I_HATE_THEM_ALL, "Mood must be I_HATE_THEM_ALL.");
    }

    @Test(description = "It is not usefull test. It is empty.", priority = 5)
    public void emptyTest() {
        System.out.println("I am empty test!");
    }

    @AfterClass
    public void endSetUp() {
        girl = null;
    }

}
