package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SendPackagePage extends PageSwitcher {

    @FindBy(xpath = ".//div[@class=\"b-datalist__item__subj\"][1]")
    WebElement lastLetter;

    public SendPackagePage(WebDriver driver) {
        super(driver);
    }

    public String getLastSendLetterSubject() {
        return lastLetter.getText();
    }
}
