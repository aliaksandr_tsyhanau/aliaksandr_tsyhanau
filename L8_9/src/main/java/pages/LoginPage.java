package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends AbstractPage {

    private static final String LOGIN_INPUT_LOCATOR = "//*[@id='mailbox:login']";//input[@id='mailbox:login']
    private static final String INCORRECT_LOGIN_MESSAGE_LOCATOR = "//*[@id='mailbox:error']";//*[@id='mailbox:error']
    private static final String LOGIN_INPUT_BUTTON_LOCATOR = "//label[@id='mailbox:submit']/input[@class='o-control']";//label[@id='mailbox:submit']//input[@class='o-control']

    @FindBy(xpath = LOGIN_INPUT_LOCATOR)
    private WebElement loginInput;
    @FindBy(id = "mailbox:password")
    private WebElement passwordInput;
    @FindBy(xpath = LOGIN_INPUT_BUTTON_LOCATOR)
    private WebElement submitButton;
    @FindBy(xpath = INCORRECT_LOGIN_MESSAGE_LOCATOR)
    private WebElement incorrectLoginMessage;

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void openPage() {
        driver.get("https://mail.ru");
    }

    public InboxPackagePage logIn(String login, String password) {
        inputLogin(login);
        inputPassword(password);
        clickSubmitLogIn();
        return new InboxPackagePage(driver);
    }

    public LoginPage errorLogIn(String login, String password) {
        inputLogin(login);
        inputPassword(password);
        clickSubmitLogIn();
        return this;
    }

    public void inputLogin(String login) {
        new WebDriverWait(driver, TEN)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOGIN_INPUT_LOCATOR)));
        loginInput.clear();
        loginInput.sendKeys(login);
    }

    public void inputPassword(String password) {
        new WebDriverWait(driver, TEN)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOGIN_INPUT_LOCATOR)));
        passwordInput.clear();
        passwordInput.sendKeys(password);
        submitButton.click();
    }

    public void clickSubmitLogIn() {
        new WebDriverWait(driver, TEN)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(LOGIN_INPUT_LOCATOR)));
        submitButton.click();
    }

    public String getIncorrectLoginMessageText() {
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(INCORRECT_LOGIN_MESSAGE_LOCATOR)));
        return incorrectLoginMessage.getText();
    }
}
