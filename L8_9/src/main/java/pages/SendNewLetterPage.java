package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SendNewLetterPage extends AbstractPage {

    private static final String INBOX_PACKAGE_LOCATOR =
            "//span[@class='b-nav__item__text b-nav__item__text_unread']";
    @FindBy(xpath = "//input[@id='compose_to']//following-sibling::*[3]")
    WebElement letterAddress;
    @FindBy(xpath = "//input[@name=\"Subject\"]")
    WebElement letterSubject;
    @FindBy(id = "tinymce")
    WebElement letterBody;
    @FindBy(xpath = "//div[@data-name=\"send\"]//span")
    WebElement letterSendButton;
    @FindBy(xpath = "(//div[@id='b-toolbar__right']//span[@class='b-toolbar__btn__text'])[1]")
    //(//*[@id='MailRuConfirm']//button[@class=\"btn btn_stylish btn_main confirm-ok\"])[3]")
            WebElement letterContinueButton;
    @FindBy(xpath = "(//button[@class='btn btn_stylish btn_main confirm-ok']//span[@class='btn__text'])[2]")
    WebElement inboxAlertButton;
    @FindBy(xpath = INBOX_PACKAGE_LOCATOR)
    WebElement inboxPackage;

    public SendNewLetterPage(WebDriver driver) {
        super(driver);
    }

    public void clickInboxButton() {

        try {
//            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//button[@class='btn btn_stylish btn_main confirm-ok']//span[@class='btn__text'])[2]")));
        } finally {
            /*if (inboxAlertButton.isDisplayed()) {
                inboxAlertButton.click();
            }*/
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath
                    ("//span[@class='b-nav__item__text b-nav__item__text_unread']")));
            inboxPackage.click();;
        }
    }

    public void sendLetter(String lettersAddress, String lettersSubject, String lettersText) {
        letterAddress.sendKeys(lettersAddress);
        letterSubject.sendKeys(lettersSubject);
        driver.switchTo().frame(0);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("tinymce")));
        letterBody.sendKeys(lettersText);
        driver.switchTo().defaultContent();
        letterSendButton.click();
    }

    public String sendLetter() {
        sendLetter("", "", "");
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        alert.accept();
        return alertText;
    }

    public void sendLetter(String lettersAddress) {
        sendLetter(lettersAddress, "", "");
        letterContinueButton.click();
    }
}
