package tests;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.*;

public class SendMailWithoutSubjectAndBodyTest extends BeforeSendTest {
    @Test(description = "After sending letter to correct "
            + "address, but without subject and body text we will see this letter in Sent and Inbox folders.")
    public void sendMailWithoutSubjectAndBodyTest() {

        InboxPackagePage inboxPackagePage = new InboxPackagePage(driver);
        inboxPackagePage.clickComposeButton();

        SendNewLetterPage sendNewLetterPage = new SendNewLetterPage(driver);
        sendNewLetterPage.sendLetter(CORRECT_ADDRESS);

        SoftAssert softAssert = new SoftAssert();

        sendNewLetterPage.clickInboxButton();

        softAssert.assertTrue(inboxPackagePage.getLastLetterSubject().contains("")
                        || inboxPackagePage.getLastLetterSubject().contains("<Без темы>"),
                "Subject and body are not as in sended letter.");

        inboxPackagePage.clickSendButton();
        SendPackagePage sendPackagePage = new SendPackagePage(driver);

        softAssert.assertTrue(sendPackagePage.getLastSendLetterSubject().contains("")
                        || sendPackagePage.getLastSendLetterSubject().contains("<Без темы>"),
                "Subject and body are not as in sended letter.");

        softAssert.assertAll();
    }
}
