package tests;

import org.testng.annotations.BeforeClass;
import pages.InboxPackagePage;
import pages.LoginPage;

public class BeforeSendTest extends BeforeAfterTest {

    @BeforeClass(description = "log in mail box with correct name and password")
    public void logInMailRu() {

        LoginPage loginPage = new LoginPage(driver);
        loginPage.openPage();
        InboxPackagePage inboxPackagePage = loginPage.logIn(CORRECT_NAME, CORRECT_PASS);
    }
}
