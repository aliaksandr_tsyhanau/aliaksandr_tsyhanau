package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;

public class SendAndDeleteMailTest extends BeforeSendTest {

    private static final String DRAFT_SUBJECT = "Draft";
    private static final String DRAFT_TEXT = "draft";

    @Test(description = "After sending letter to correct "
            + "address, and deleting it we will see this letter in Trash folder.")
    public void sendMailTest() {

        InboxPackagePage inboxPackagePage = new InboxPackagePage(driver);
        inboxPackagePage.clickComposeButton();

        SendNewLetterPage sendNewLetterPage = new SendNewLetterPage(driver);
        sendNewLetterPage.sendLetter(CORRECT_ADDRESS, DRAFT_SUBJECT, DRAFT_TEXT);

        sendNewLetterPage.clickInboxButton();
        inboxPackagePage.clickDeleteLastLetter(DRAFT_SUBJECT);

        inboxPackagePage.clickTrashButton();
        TrashPackagePage trashPackagePage = new TrashPackagePage(driver);
        Assert.assertTrue(trashPackagePage.getLastTrashLetterSubject(DRAFT_SUBJECT),
                "Letter is not in draft or subject is incorrect");
    }

    @Test(dependsOnMethods = "sendMailTest",
            description = "Delete letter from Trash folder and check that it is not present in Trash.")
    public void deleteMailTest() {
        TrashPackagePage trashPackagePage = new TrashPackagePage(driver)
                .clickDeleteLastLetter(DRAFT_SUBJECT);
        Assert.assertFalse(trashPackagePage.getLastTrashLetterSubject(DRAFT_SUBJECT),
                "This letter must be deleted from draft");
//        Assert.assertEquals(trashPackagePage.getLastLetterSubject(), DRAFT_SUBJECT + DRAFT_TEXT,
//                "This letter must be deleted from draft");
    }
}
