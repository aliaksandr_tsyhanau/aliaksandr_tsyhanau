package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import pages.*;

public class SendMailTest extends BeforeSendTest {

    @Test(description = "After sending letter to correct address with subject"
            + " and body text we will see this letter in Sent and Inbox folders.")
    public void sendMailTest() {

        InboxPackagePage inboxPackagePage = new InboxPackagePage(driver);

        inboxPackagePage.clickComposeButton();
        SendNewLetterPage sendNewLetterPage = new SendNewLetterPage(driver);
        sendNewLetterPage.sendLetter(CORRECT_ADDRESS, CORRECT_SUBJECT, CORRECT_LETTERSTEXT);

        SoftAssert softAssert = new SoftAssert();

        sendNewLetterPage.clickInboxButton();
        softAssert.assertEquals(inboxPackagePage.getLastLetterSubject(), CORRECT_SUBJECT + CORRECT_LETTERSTEXT,
                "Letter is not in inbox. Subject and body are not correct.");

        inboxPackagePage.clickSendButton();
        SendPackagePage sendPackagePage = new SendPackagePage(driver);
        softAssert.assertEquals(sendPackagePage.getLastSendLetterSubject(), CORRECT_SUBJECT + CORRECT_LETTERSTEXT,
                "Letter is not in sended box. Subject and body are not correct.");
        softAssert.assertAll();
    }
}
