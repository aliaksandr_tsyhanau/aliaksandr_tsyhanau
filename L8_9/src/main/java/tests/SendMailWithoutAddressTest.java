package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.InboxPackagePage;
import pages.SendNewLetterPage;

public class SendMailWithoutAddressTest extends BeforeSendTest {

    private final static String EXPECTED_ERROR_MESSAGE = "Не указан адрес получателя";

    @Test(description = "Sending letter  without address will throw exeption.")
    public void sendMailWithoutAddressTest() {

        InboxPackagePage inboxPackagePage = new InboxPackagePage(driver);

        inboxPackagePage.clickComposeButton();
        SendNewLetterPage sendNewLetterPage = new SendNewLetterPage(driver);
        String errorMessage = sendNewLetterPage.sendLetter();

        Assert.assertEquals(errorMessage, EXPECTED_ERROR_MESSAGE, "Message in alert is not correct.");
    }
}
