package runners;

import org.testng.TestNG;
import java.util.Arrays;
import java.util.List;

/**
 * Created by LENOVO on 21.07.2017.
 */
public class TestRunner {
    public static void main(String[] args) {
        TestNG testNG = new TestNG();
        List<String> files = Arrays.asList("./src//main//resources//testng.xml");
        testNG.setTestSuites(files);
        testNG.run();
    }
}
