package com.epam.tat.framework.config;

public enum BrowserType {

    FIREFOX("firefox"),
    CHROME("chrome");

    private String type;

    BrowserType(String type) {
        this.type = type;
    }
}
