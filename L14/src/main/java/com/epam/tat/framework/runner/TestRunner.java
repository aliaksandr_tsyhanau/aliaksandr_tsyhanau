package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.epam.tat.logs.Log;
import org.openqa.selenium.NotFoundException;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.Arrays;
import java.util.List;

public class TestRunner {

    //    public static void main(String[] args) {
    //        TestNG testNG = new TestNG();
    //        List<String> files = Arrays.asList("./src//main//resources//testngMail.xml");//,
    //               // "./src//main//resources//testngCloud.xml");
    //        testNG.setTestSuites(files);
    //       // testNG.addListener(new TestListener());
    //
    //        PropertyConfigurator.configureAndWatch("./src/log4j.properties");
    //        testNG.run();
    //    }

    public static TestNG configureTestNG() {
        TestNG testNG = new TestNG();
        List<String> files = Parameters.instance().getSuits();
        testNG.setTestSuites(files);
        testNG.setParallel(XmlSuite.ParallelMode.valueOf(Parameters.instance().getParallelMode()));
        testNG.setThreadCount(Parameters.instance().getThreadCount());
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse cli");
        parseCli(args);
        Log.info("Start app ...");
        configureTestNG().run();
        Log.info("End app.");
    }

    public static void parseCli(String[] args) {
        Log.info("Parse clis using JCommander.");
        JCommander jCommander = new JCommander(Parameters.instance());
        try {
            jCommander.parse(args);
        } catch (NotFoundException e) {
            Log.error(e.getMessage(), e);
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}
