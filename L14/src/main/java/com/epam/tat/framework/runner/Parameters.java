package com.epam.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.tat.framework.config.BrowserType;

import java.util.ArrayList;
import java.util.List;

public class Parameters {

    private static final int FOUR = 4;

    private static Parameters instance;

    @Parameter(names = {"--chrome", "-c"}, description = "Path to Google Chrome driver")
    private String chromeDriver = ".\\src\\main\\resources\\chromedriver.exe";

    @Parameter(names = {"--firefox", "-f"}, description = "Path to Firefox Gecko driver")
    private String geckoDriver = ".\\src\\main\\resources\\geckodriver.exe";

    @Parameter(names = {"--threads", "-t"}, description = "Count of threads")
    private int threadCount;

    @Parameter(names = {"--parallel", "-p"}, description = "Type of parallel mode.")
    private String parallelMode = "classes";

    @Parameter(names = "--help", help = true, description = "How to use")
    private boolean help;

    @Parameter(names = {"--browser", "-b"}, required = true, description = "Browser type",
            converter = BrowserTypeConverter.class)
    private BrowserType browserType;

    @Parameter(description = "Suites for launch", names = {"--suits", "-s"})
    private List<String> suits = new ArrayList<>();

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public List<String> getSuits() {
        return suits;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public String getParallelMode() {
        return parallelMode.toUpperCase();
    }

    public String getChromeDriver() {
        return chromeDriver;
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public static class BrowserTypeConverter implements IStringConverter<BrowserType> {

        @Override
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}
