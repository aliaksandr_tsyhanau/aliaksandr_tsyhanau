package com.epam.tat.framework.listeners;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.logs.Log;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class TestListener extends TestListenerAdapter {

    public void onStart(ITestContext iTestContext) {

        // onStart(iTestContext);
        Log.info("[TEST STARTED]" + iTestContext.getName());

    }

    public void onFinish(ITestContext iTestContext) {

        //onFinish(iTestContext);
        Log.info("[TEST FINISHED]" + iTestContext.getName());
        //        Log.debug("Quits this driver, closes all windows.");
        Browser.getBrowser().close();

    }

    public void onTestFailure(ITestResult tr) {
        //onTestFailure(tr);
        Browser.getBrowser().screenshot();
        Log.error("Test is failed");
    }
}
