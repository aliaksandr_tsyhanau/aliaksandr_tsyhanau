package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    public static final int SIXTY = 60;

    public static WebDriver getWebDriver() {

        WebDriver driver;

        switch (Parameters.instance().getBrowserType()) {
            case FIREFOX:
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
                driver = new FirefoxDriver();
                //driver.manage().window().maximize();
                break;
            case CHROME:
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(SIXTY, TimeUnit.SECONDS);
                driver.manage().timeouts().pageLoadTimeout(SIXTY, TimeUnit.SECONDS);
                break;
            default:
                throw new RuntimeException("No support for: " + Parameters.instance().getBrowserType());
        }
        // System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        //  driver = new ChromeDriver();
        //driver.manage().window().maximize();
        //        driver.manage().timeouts().implicitlyWait(SIXTY, TimeUnit.SECONDS);
        //        driver.manage().timeouts().pageLoadTimeout(SIXTY, TimeUnit.SECONDS);
        //                EventFiringWebDriver eventFiringDriver = new EventFiringWebDriver(driver);
        //                eventFiringDriver.register(new WebDriverListener());
        //                return eventFiringDriver;
        return driver;
    }
}
