package com.epam.tat.framework.listeners;

import com.epam.tat.logs.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

public class WebDriverListener extends AbstractWebDriverEventListener {

    public void afterNavigateTo(String url, WebDriver driver) {
        Log.debug("WebDriverListener... Webdriver navigated to url: " + url);
        //Browser.getBrowser().screenshot();
    }

    public void beforeFindBy(By by, WebElement element, WebDriver driver) {
        Log.debug("Before find " + element);
    }

    public void beforeClickOn(WebElement element, WebDriver driver) {
        Log.debug("Before click on " + element);
    }
}
