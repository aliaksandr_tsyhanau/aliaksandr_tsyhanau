package com.epam.tat.bo;

public class AccountBuilder {

    private Account account = new Account();

    public AccountBuilder name(String name) {
        account.setName(name);
        return this;
    }

    public AccountBuilder password(String password) {
        account.setPassword(password);
        return this;
    }

    public Account build() {
        return account;
    }
}
