package com.epam.tat.test;


import com.epam.tat.service.LogInService;

import org.testng.annotations.AfterClass;

public class BeforeAfterTest {

    @AfterClass(description = "Close browser (all opened pages) and quit.")
    public void quitBrowser() {
        LogInService.quitBrowser();
    }
}
