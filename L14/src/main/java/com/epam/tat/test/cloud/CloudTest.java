package com.epam.tat.test.cloud;

import com.epam.tat.bo.Account;
import com.epam.tat.bo.AccountFactory;
import com.epam.tat.bo.ItemInCloud;
import com.epam.tat.bo.ItemInCloudFactory;
import com.epam.tat.page.cloud.MainCloudPage;
import com.epam.tat.service.CloudService;
import com.epam.tat.service.LogInService;
import com.epam.tat.test.BeforeAfterTest;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CloudTest extends BeforeAfterTest {

    @Test(description = "Correct input of login and password opens mail.ru cloud.")
    public void positiveLoginTest() {
        Account account = AccountFactory.getExistentAccount();
        LogInService.logInCloud(account);
        MainCloudPage mainCloudPage = new MainCloudPage();
        Assert.assertEquals(mainCloudPage.getEmailText(), account.getName() + "@mail.ru",
                "Login error. Check that login and password are correct.");
    }

    @Test(dependsOnMethods = "positiveLoginTest", description = "Creating of new folder.")
    public void createNewFolderTest() {
        ItemInCloud itemInCloud = ItemInCloudFactory.getFolderInCloud();
        CloudService.createFolder(itemInCloud);
        Assert.assertTrue(CloudService.checkItemInRoot(itemInCloud),
                "Folder " + itemInCloud.getName() + " is not created.");
    }

    @Test(dependsOnMethods = "createNewFolderTest", description = "Check, that new folder was created.")
    public void deleteFolderTest() {

        ItemInCloud itemInCloud = ItemInCloudFactory.getFolderInCloud();
        CloudService.deleteFolder(itemInCloud);

        Assert.assertFalse(CloudService.checkItemInRoot(itemInCloud),
                "Folder " + itemInCloud.getName() + " is not removed.");
    }

    @Test(dependsOnMethods = "positiveLoginTest",
            description = "Upload new file to root of cloud and check this handling.")
    public void uploadFileTest() {
        ItemInCloud itemInCloud = ItemInCloudFactory.getFileInCloud();
        CloudService.uploadFile(itemInCloud);

        Assert.assertTrue(CloudService.checkItemInRoot(itemInCloud),
                "File " + itemInCloud.getName() + " is not created.");
    }

    @Test(dependsOnMethods = "uploadFileTest", description = "Drug and drop uploaded file to folder."
            + " Check, that this actions was succesfull.")
    public void dragAndDropFileTest() {
        ItemInCloud itemInCloudFolder = ItemInCloudFactory.getFolderInCloud();
        CloudService.createFolder(itemInCloudFolder);
        ItemInCloud itemInCloudFile = ItemInCloudFactory.getFileInCloud();
        CloudService.dragAndDropFile(itemInCloudFile, itemInCloudFolder);

        Assert.assertTrue(CloudService.checkItemInFolder(itemInCloudFile),
                "File " + itemInCloudFile.getName() + " is not moved to folder.");
    }

    @Test(dependsOnMethods = "dragAndDropFileTest",
            description = "Get link of uploaded file, open it and check,that correct file was opened.")
    public void getLinkForUploadedFileTest() {

        ItemInCloud itemInCloud = ItemInCloudFactory.getFileInCloud();

        Assert.assertEquals(CloudService.getNameOfLinkOpensFile(itemInCloud),
                itemInCloud.getName() + ".txt");
    }
}
