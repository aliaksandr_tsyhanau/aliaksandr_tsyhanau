package com.epam.tat.service;

import com.epam.tat.bo.ItemInCloud;
import com.epam.tat.logs.Log;
import com.epam.tat.page.cloud.MainCloudPage;
import com.epam.tat.page.cloud.OpenedLinkCloudPage;

public class CloudService {

    public static void createFolder(ItemInCloud itemInCloud) {
        Log.info("Create new Folder named " + itemInCloud.getName());
        new MainCloudPage()
                .clickCreateNewSelectButton()
                .clickChooseFolderInSelectButton()
                .inputNameOfNewFolder(itemInCloud.getName())
                .clickAddButton();
    }

    public static boolean checkItemInRoot(ItemInCloud itemInCloud) {
        Log.info("Check that item " + itemInCloud.getName() + "exists.");
        return new MainCloudPage().checkItemInRoot(itemInCloud.getName());
    }

    public static boolean checkItemInFolder(ItemInCloud itemInCloud) {
        Log.info("Check that item " + itemInCloud.getName() + "exists.");
        return new MainCloudPage().checkItemInFolder(itemInCloud.getName());
    }

    public static void deleteFolder(ItemInCloud itemInCloud) {
        Log.info("Delete item " + itemInCloud.getName());
        new MainCloudPage()
                .clickCheckbox(itemInCloud)
                .clickDeleteButton()
                .clickAlertButton()
                .clickConfirmButton();
    }

    public static void uploadFile(ItemInCloud itemInCloud) {
        Log.info("Upload file " + itemInCloud.getName());
        new MainCloudPage()
                .clickUploadFileButton()
                .chooseAndUploadFile(itemInCloud);
    }

    public static void dragAndDropFile(ItemInCloud itemInCloudFile, ItemInCloud itemInCloudFolder) {
        Log.info("Drag and drop file  " + itemInCloudFile.getName() + " to folder " + itemInCloudFolder.getName());
        new MainCloudPage()
                .dragAndDropFile(itemInCloudFile, itemInCloudFolder)
                .clickMoveConfirmButton();
    }

    public static String getNameOfLinkOpensFile(ItemInCloud itemInCloud) {
        Log.info("Get name of " + itemInCloud.getName());
        new MainCloudPage().openLinkOfUploadedFile(itemInCloud.getName());
        return new OpenedLinkCloudPage().getNameOfLinkOpensFile();
    }
}
