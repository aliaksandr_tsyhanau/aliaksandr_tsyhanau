package com.epam.tat.service;

import com.epam.tat.bo.Letter;
import com.epam.tat.logs.Log;
import com.epam.tat.page.mail.*;

public class MailService {

    public static void sendNewLetter(Letter letter) {

        new InboxPackagePage().clickComposeButton();
        new SendNewLetterPage()
                .fillLettersAddress(letter.getEmail())
                .fillLettersSubject(letter.getSubject())
                .fillLettersText(letter.getBodyText())
                .clickSendButton();

    }

    public static String sendNewLetterWithoutAddress(Letter letter) {
        Log.info("Send new letter without address.");
        sendNewLetter(letter);

        return new SendNewLetterPage().getTextFromAlert();
    }

    public static void sendNewLetterWithoutSubjectAndText(Letter letter) {
        Log.info("Send new letter without subject and body with email: " + letter.getEmail());
        sendNewLetter(letter);
        new SendNewLetterPage().clickContinueButton();
    }

    public static void clickInboxButton() {
        Log.info("Move to inbox.");
        new SendNewLetterPage().clickInboxButton();
    }

    public static boolean getLastLetterSubject(String subject) {
        Log.info(String.format("Check that letter with subject and body <%s> exists.", subject));
        return new PageSwitcher().checkThatLetterHasSubject(subject);
    }

    public static void moveToSended() {
        Log.info("Move to Sended box");
        new PageSwitcher().clickSendButton();
    }

    public static boolean getLastSendLetterSubject(String subject) {
        Log.info(String.format("Check that letter with subject and body <%s> exists.", subject));
        return new SendPackagePage().checkLetterHasSubject(subject);
    }

    public static void deleteLastLetter(Letter letter) {
        Log.info(String.format("Delete letter with subject <%s> and address <%s>.", letter.getSubject(),
                letter.getEmail()));
        new InboxPackagePage().clickDeleteLastLetter(letter.getSubject());
    }

    public static void moveToTrash() {
        Log.info("Move to Trash box");
        new InboxPackagePage().clickTrashButton();
    }

    public static boolean getLastTrashLetterSubject(Letter letter) {
        //return new TrashPackagePage().getLastTrashLetterSubject(letter.getSubject());
        Log.info(String.format("Check that letter with subject and body <%s> exists.", letter.getSubject()));
        return new TrashPackagePage().checkThatLetterHasSubject(letter.getSubject());
    }

    public static void clickInboxBtn() {
        Log.info("Move to Inbox box");
        new PageSwitcher().clickInboxBtn();
    }
}
