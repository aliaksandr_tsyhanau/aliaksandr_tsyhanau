package com.epam.tat.page.mail;

import org.openqa.selenium.By;

public class TrashPackagePage extends PageSwitcher {

    private static final String CHECK_BOX_TRASH_LOCATOR =
            "//*[not(contains(@style, 'display: none'))]//div[contains(@class, 'b-datalist__item__subj')"
                    + " and text()='%s']/ancestor::a//div[contains(@class, 'js-item-checkbox')]"
                    + "//div[@class='b-checkbox__box']";
    private static final String LETTER_SUBJECT_TRASH_LOCATOR =
            "//div[contains(@class, 'b-datalist__item__subj')  and text()='%s']";
    private static final By DELETE_BUTTON = By.xpath("(//div[@data-name='remove'])[2]");

    public TrashPackagePage() {
        super();
    }

    public TrashPackagePage clickDeleteLastLetter(String subject) {
        //browser.clickAllElements(CHECK_BOX_TRASH_LOCATOR, subject);
        By locator = By.xpath(String.format(CHECK_BOX_TRASH_LOCATOR, subject));
        browser.clickAllElements(locator);
        browser.waitForElementIsVisible(DELETE_BUTTON);
        browser.click(DELETE_BUTTON);
        return this;
    }

    public boolean getLastTrashLetterSubject(String subject) {
        return browser.findElements(By.xpath(String.format(
                LETTER_SUBJECT_TRASH_LOCATOR, subject))).size() > 0;
    }
}
