package com.epam.tat.page;

import com.epam.tat.framework.ui.Browser;

public class AbstractPage {

    protected Browser browser;

    public AbstractPage() {
        this.browser = Browser.getBrowser();
    }
}

